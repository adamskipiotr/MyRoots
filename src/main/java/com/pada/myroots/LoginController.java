package com.pada.myroots;


import com.pada.myroots.admin.AdminFacade;
import com.pada.myroots.client.ClientDTO;
import com.pada.myroots.client.ClientFacade;
import com.pada.myroots.employee.EmployeeFacade;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@CrossOrigin("*")
@RestController
@AllArgsConstructor
public class LoginController {

    private final ClientFacade clientFacade;
    private final EmployeeFacade employeeFacade;
    private final AdminFacade adminFacade;

    @GetMapping("/login")
    public String loginUser(Principal principal) {
        String login = principal.getName();
        Object user;

        user = clientFacade.findByLogin(login);

        if (user != null) {
            return "client";
        } else {
            user = employeeFacade.findByLogin(login);
        }

        if (user != null) {
            return "employee";
        } else {
            user = adminFacade.findByLogin(login);
        }
        if (user != null) {
            return "admin";
        }
        return "empty";
    }

    @GetMapping("/logout")
    public void logoutUser() {
    }

    @PostMapping("/registerClient")
    public void registerUser(@RequestBody ClientDTO dto) {
        clientFacade.addUser(dto);
    }
}
