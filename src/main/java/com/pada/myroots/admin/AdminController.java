package com.pada.myroots.admin;

import com.pada.myroots.employee.dto.EmployeeContractsInfoDTO;
import com.pada.myroots.employee.dto.EmployeeDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@Controller
@RequestMapping("admin")
@AllArgsConstructor
public class AdminController {

    private final AdminService adminService;

    @PostMapping("/addEmployee")
    @ResponseBody
    public void addEmployee(@RequestBody EmployeeDTO employeeDTO) {
        adminService.addNewEmployee(employeeDTO);
    }

    @GetMapping("/getContracts")
    @ResponseBody
    public List<EmployeeContractsInfoDTO> getEmployeesStats() {
        return adminService.getAllEmployeesStats();
    }

    @GetMapping("/deactivate/{employeeID}")
    public void deactivateEmployeeAccount(@PathVariable("employeeID") Long employeeID) {
        adminService.deactivateEmployeeAccount(employeeID);
    }

    @DeleteMapping("/delete/{employeeID}")
    public void deleteEmployeeAccount(@PathVariable("employeeID") Long employeeID) {
        adminService.deleteEmployeeAccount(employeeID);
    }
}
