package com.pada.myroots.admin;


import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class AdminFacade {

    private final AdminService adminService;

    public Admin findByLogin(String login) {
        return adminService.findByLogin(login);
    }

    public Optional<Admin> findByLoginAuth(String login) {
        return adminService.findByLoginAuth(login);
    }
}
