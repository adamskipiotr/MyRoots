package com.pada.myroots.admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
interface AdminRepository extends JpaRepository<Admin, Long> {


    Optional<Admin> findByLogin(String login);
}
