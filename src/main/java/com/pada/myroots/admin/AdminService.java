package com.pada.myroots.admin;


import com.pada.myroots.domain.application.ApplicationFacade;
import com.pada.myroots.employee.Employee;
import com.pada.myroots.employee.EmployeeFacade;
import com.pada.myroots.employee.dto.EmployeeContractsInfoDTO;
import com.pada.myroots.employee.dto.EmployeeDTO;
import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@Data
class AdminService {

    private final EmployeeFacade employeeFacade;
    private final AdminRepository adminRepository;
    private final ApplicationFacade applicationFacade;
    private final PasswordEncoder passwordEncoder;


    public void addNewEmployee(EmployeeDTO employeeDTO) {
        String encryptedPassword = passwordEncoder.encode(employeeDTO.getPassword());
        Employee newEmployee = new Employee();
        newEmployee.setValuesFromDTO(employeeDTO, encryptedPassword);
        employeeFacade.addNewEmployee(newEmployee);
    }

    public Admin findByLogin(String login) {
        return adminRepository.findByLogin(login).orElse(null);
    }

    public List<EmployeeContractsInfoDTO> getAllEmployeesStats() {
        List<Employee> employees = employeeFacade.getAllEmployeesStats();
        List<EmployeeContractsInfoDTO> employeesStats = new LinkedList<>();
        for (Employee employee : employees) {
            Long activeContractsCount = applicationFacade.countAllActiveContractsForEmployee(employee);
            String additionalInfo;
            if (employee.isActive()) {
                additionalInfo = "Konto aktywne";
            } else {
                additionalInfo = "Konto nieaktywne";
            }
            employeesStats.add(new EmployeeContractsInfoDTO(employee, "Ilość aktywnych procesów:" + activeContractsCount.toString(), additionalInfo));
        }
        return employeesStats;
    }

    public Optional<Admin> findByLoginAuth(String login) {
        return adminRepository.findByLogin(login);
    }

    public void deactivateEmployeeAccount(Long employeeID) {
        employeeFacade.deactivateEmployeeAccount(employeeID);
    }

    public void deleteEmployeeAccount(Long employeeID) {
        employeeFacade.deleteEmployeeAccount(employeeID);
    }
}
