package com.pada.myroots.client;

import com.pada.myroots.domain.application.Application;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
    private boolean isActive;
    private String roles;

    @OneToOne
    @NonNull
    private Application application;

    public void setValuesFromDTO(ClientDTO dto, String password) {
        this.name = dto.getName();
        this.lastName = dto.getLastName();
        this.login = dto.getLogin();
        this.password = password;
        this.email = dto.getEmail();
        this.isActive = true;
        this.roles = "ROLE_CLIENT";
    }
}
