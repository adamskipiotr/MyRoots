package com.pada.myroots.client;

import com.pada.myroots.domain.application.ApplicationFacade;
import com.pada.myroots.domain.correctLayoutData.CorrectTreeLayoutData;
import com.pada.myroots.domain.correctLayoutData.CorrectTreeLayoutFacade;
import com.pada.myroots.domain.familyMember.*;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberGraphWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@CrossOrigin("*")
@Controller
@RequestMapping("client")
@AllArgsConstructor
public class ClientController {

    private final ClientFacade clientFacade;
    private final ApplicationFacade applicationFacade;
    private final FamilyMemberFacade familyMemberFacade;
    private final CorrectTreeLayoutFacade correctTreeLayoutFacade;

    @GetMapping("/createRequest")
    @ResponseBody
    public void applicationRequest(Principal principal) {
        String userName = principal.getName();
        Client client = clientFacade.findByLogin(userName);
        applicationFacade.createApplicationRequest(client);
    }

    @GetMapping("/getRequestStatus")
    @ResponseBody
    public String getRequestStatus(Principal principal) {
        String userName = principal.getName();
        Client client = clientFacade.findByLogin(userName);
        return applicationFacade.getClientRequestStatus(client);
    }

    @GetMapping("/getFamilyTree")
    @ResponseBody
    public FamilyMemberGraphWrapper getFamilyTree(Principal principal) {
        String userName = principal.getName();
        Client client = clientFacade.findByLogin(userName);
        return familyMemberFacade.getAllFamilyMembersGraph(client);
    }

    @GetMapping("/getTreeName")
    @ResponseBody
    public String getFamilyTreeName(Principal principal) {
        String userName = principal.getName();
        Client client = clientFacade.findByLogin(userName);
        return clientFacade.getFamilyTreeName(client);
    }

    @GetMapping("/getTreeLayoutCorrections")
    @ResponseBody
    public List<CorrectTreeLayoutData> getTreeLayoutCorrections(Principal principal) {
        String userName = principal.getName();
        Client client = clientFacade.findByLogin(userName);
        return correctTreeLayoutFacade.getAllCorrectionsForFamilyTreeClient(client);
    }
}
