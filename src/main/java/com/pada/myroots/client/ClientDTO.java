package com.pada.myroots.client;


import lombok.Data;

@Data
public class ClientDTO {

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
}
