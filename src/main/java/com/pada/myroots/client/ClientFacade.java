package com.pada.myroots.client;

import com.pada.myroots.domain.application.Application;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class ClientFacade {

    private final ClientService clientService;

    public Client findByLogin(String login) {
        return clientService.findByLogin(login);
    }

    public void addUser(ClientDTO dto) {
        clientService.addClient(dto);
    }

    public void addApplicationToClient(Long id, Application application) {
        clientService.addApplicationToClient(id, application);
    }

    public Optional<Client> findByLoginAuth(String login) {
        return clientService.findByLoginAuth(login);
    }

    public String getFamilyTreeName(Client client) {
        Application application = client.getApplication();
        return application.getFamilyTreeName();
    }
}
