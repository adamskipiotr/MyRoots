package com.pada.myroots.client;

import com.pada.myroots.domain.application.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
interface ClientRepository extends JpaRepository<Client, Long> {

    Optional<Client> findByLogin(String login);

    @Transactional
    @Modifying
    @Query("UPDATE Client c set c.application = :application where c.id = :clientID")
    void addApplicationToClient(@Param("clientID") Long id, @Param("application") Application application);
}
