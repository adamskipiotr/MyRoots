package com.pada.myroots.client;


import com.pada.myroots.domain.application.Application;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
class ClientService {

    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;

    public Client findByLogin(String login) {
        Optional<Client> clientOptional = clientRepository.findByLogin(login);
        return clientOptional.orElse(null);
    }

    public void addClient(ClientDTO dto) {
        String encryptedPassword = passwordEncoder.encode(dto.getPassword());
        Client client = new Client();
        client.setValuesFromDTO(dto, encryptedPassword);
        clientRepository.save(client);
    }

    public void addApplicationToClient(Long id, Application application) {
        clientRepository.addApplicationToClient(id, application);
    }

    public Optional<Client> findByLoginAuth(String login) {
        return clientRepository.findByLogin(login);
    }
}
