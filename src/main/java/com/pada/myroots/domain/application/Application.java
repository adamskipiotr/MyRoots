package com.pada.myroots.domain.application;


import com.pada.myroots.client.Client;
import com.pada.myroots.employee.Employee;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private UUID applicationUUID;
    private String status;

    @OneToOne(mappedBy = "application")
    private Client client;

    @ManyToOne
    private Employee employee;
    private String providedInformations;
    private String familyTreeName;

    public Application(Client client) {
        this.applicationUUID = UUID.randomUUID();
        this.status = "nierozpoczety";
        this.client = client;
        this.familyTreeName = client.getLastName();
        this.providedInformations = client.getEmail();
    }
}
