package com.pada.myroots.domain.application;

import com.pada.myroots.client.Client;
import com.pada.myroots.domain.application.DTO.ApplicationDto;
import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.employee.Employee;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class ApplicationFacade {

    private final ApplicationService applicationService;

    public List<ApplicationDto> getAllUnhandledApplications() {
        return applicationService.getAllUnhandledApplications();
    }

    public void createApplicationRequest(Client client) {
        applicationService.createApplicationRequest(client);
    }

    public void assignToApplication(String UUID, Employee assignedEmployee) {
        applicationService.assignToApplication(UUID, assignedEmployee);
    }

    public List<ClaimedApplicactionDto> getClaimedRequests(Employee assignedEmployee) {
        return applicationService.getClaimedRequests(assignedEmployee);
    }

    public String getClientRequestStatus(Client client) {
        return applicationService.getClientRequestStatus(client);
    }

    public void finishCreatingTree(String familyTreeName) {
        applicationService.finishCreatingTree(familyTreeName);
    }

    public Long countAllActiveContractsForEmployee(Employee employee) {
        return applicationService.countAllActiveContractsForEmployee(employee);
    }

    public String getFamilyTreeNameByUUID(UUID uuid) {
        return applicationService.getFamilyTreeNameByUUID(uuid);
    }
}
