package com.pada.myroots.domain.application;

import com.pada.myroots.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    @Transactional
    @Modifying
    @Query("UPDATE Application app SET app.employee = :employee, app.status = 'wrealizacji' WHERE app.applicationUUID = :applicationUUID")
    void assignToApplication(@Param("applicationUUID") UUID applicationUUID, @Param("employee") Employee assignedEmployee);

    @Query("SELECT app FROM Application app WHERE  app.employee IS NULL")
    List<Application> findAllUnhadledApplications();

    @Query("SELECT app FROM Application app WHERE  app.employee = :assignedEmployee")
    List<Application> findAllClaimedByEmployee(@Param("assignedEmployee") Employee assignedEmployee);

    @Transactional
    @Modifying
    @Query("UPDATE Application app SET app.status = 'zakonczony' where app.familyTreeName = :familyTreeName")
    void finishCreatingTree(@Param("familyTreeName") String familyTreeName);

    @Query("SELECT COUNT(app.id) FROM Application app WHERE app.employee = :employee AND app.status NOT LIKE 'zakonczony'")
    Long countAllActiveContractsForEmployee(@Param("employee") Employee employee);

    @Query("SELECT app.familyTreeName FROM Application app WHERE app.applicationUUID = :applicationUUID")
    String getFamilyTreeNameByUUID(@Param("applicationUUID") UUID applicationUUID);

    @Query("SELECT COUNT(app.familyTreeName) FROM Application app WHERE app.familyTreeName LIKE :familyTreeName" + "%")
    Long checkLastName(@Param("familyTreeName") String familyTreeName);
}
