package com.pada.myroots.domain.application;

import com.pada.myroots.client.Client;
import com.pada.myroots.client.ClientFacade;
import com.pada.myroots.domain.application.DTO.ApplicationDto;
import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.employee.Employee;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
class ApplicationService {

    private final ApplicationRepository applicationRepository;
    private final ClientFacade clientFacade;

    public List<ApplicationDto> getAllUnhandledApplications() {
        List<Application> allApplications = applicationRepository.findAllUnhadledApplications();
        return allApplications
                .stream()
                .filter(application -> application.getStatus().equals("nierozpoczety"))
                .map(ApplicationDto::new)
                .collect(Collectors.toList());
    }

    public void createApplicationRequest(Client client) {
        Application application = new Application(client);
        Long count = applicationRepository.checkLastName(client.getLastName());
        if (count > 0) {
            application.setFamilyTreeName(client.getLastName() + count);
        }
        applicationRepository.save(application);
        clientFacade.addApplicationToClient(client.getId(), application);
    }

    public void assignToApplication(String applicationUUID, Employee assignedEmployee) {
        applicationRepository.assignToApplication(UUID.fromString(applicationUUID), assignedEmployee);
    }

    public List<ClaimedApplicactionDto> getClaimedRequests(Employee assignedEmployee) {
        List<Application> applications = applicationRepository.findAllClaimedByEmployee(assignedEmployee);
        return applications
                .stream()
                .map(ClaimedApplicactionDto::new)
                .collect(Collectors.toList());
    }

    public String getClientRequestStatus(Client client) {
        Application applicationOptional = client.getApplication();
        return Optional.ofNullable(applicationOptional)
                .map(Application::getStatus)
                .orElse("niewyslany");
    }

    public void finishCreatingTree(String familyTreeName) {
        applicationRepository.finishCreatingTree(familyTreeName);
    }

    public Long countAllActiveContractsForEmployee(Employee employee) {
        return applicationRepository.countAllActiveContractsForEmployee(employee);
    }

    public String getFamilyTreeNameByUUID(UUID uuid) {
        return applicationRepository.getFamilyTreeNameByUUID(uuid);
    }
}
