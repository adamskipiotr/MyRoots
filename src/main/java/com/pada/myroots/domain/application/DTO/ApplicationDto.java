package com.pada.myroots.domain.application.DTO;


import com.pada.myroots.domain.application.Application;
import lombok.Data;

import java.util.UUID;

@Data
public class ApplicationDto {

    private UUID applicationUUID;
    private String status;
    private String providedInformations;
    private String familyTreeName;

    public ApplicationDto(Application application) {
        this.applicationUUID = application.getApplicationUUID();
        this.status = application.getStatus();
        this.providedInformations = application.getProvidedInformations();
        this.familyTreeName = application.getFamilyTreeName();
    }
}
