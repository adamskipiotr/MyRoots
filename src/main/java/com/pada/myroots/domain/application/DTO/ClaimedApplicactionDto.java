package com.pada.myroots.domain.application.DTO;

import com.pada.myroots.domain.application.Application;
import lombok.Data;
import lombok.NonNull;

@Data
public class ClaimedApplicactionDto {
    @NonNull
    private final String familyTreeName;
    private final String applicationUUID;

    public ClaimedApplicactionDto(Application application) {
        this.familyTreeName = application.getFamilyTreeName();
        this.applicationUUID = application.getApplicationUUID().toString();
    }

    public ClaimedApplicactionDto(String testFamily, String uuid) {
        this.familyTreeName = testFamily;
        this.applicationUUID = uuid;
    }
}
