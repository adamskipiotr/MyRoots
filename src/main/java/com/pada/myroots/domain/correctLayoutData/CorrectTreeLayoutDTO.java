package com.pada.myroots.domain.correctLayoutData;


import lombok.Data;

@Data
public class CorrectTreeLayoutDTO {
    private String personToMove;
    private String direction;
    private String moveValue;
    private String familyTreeName;
}
