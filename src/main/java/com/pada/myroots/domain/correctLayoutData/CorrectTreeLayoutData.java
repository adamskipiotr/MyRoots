package com.pada.myroots.domain.correctLayoutData;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CorrectTreeLayoutData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private String personToMove;
    @NonNull
    private String direction;
    @NonNull
    private String moveValue;
    @NonNull
    private String familyTreeName;

}
