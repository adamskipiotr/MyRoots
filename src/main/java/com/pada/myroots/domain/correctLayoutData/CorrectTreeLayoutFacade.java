package com.pada.myroots.domain.correctLayoutData;

import com.pada.myroots.client.Client;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class CorrectTreeLayoutFacade {
    private final CorrectTreeLayoutService correctTreeLayoutService;

    public void addCorrections(List<CorrectTreeLayoutDTO> correctTreeLayoutDTOs) {
        correctTreeLayoutService.addCorrections(correctTreeLayoutDTOs);
    }

    public List<CorrectTreeLayoutData> getAllCorrectionsForFamilyTree(String familyTreeName) {
        return correctTreeLayoutService.getAllCorrectionsForFamilyTree(familyTreeName);
    }

    public List<CorrectTreeLayoutData> getAllCorrectionsForFamilyTreeClient(Client client) {
        return correctTreeLayoutService.getAllCorrectionsForFamilyTreeClient(client);
    }

    public List<CorrectTreeLayoutData> getAllCorrectionsForFamilyTreeEmployee(String familyTreeName) {
        return correctTreeLayoutService.getAllCorrectionsForFamilyTreeEmployee(familyTreeName);
    }
}
