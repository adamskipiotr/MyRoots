package com.pada.myroots.domain.correctLayoutData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface CorrectTreeLayoutRepository extends JpaRepository<CorrectTreeLayoutData, Long> {

    @Query("SELECT ct FROM CorrectTreeLayoutData ct WHERE ct.familyTreeName = :familyTreeName")
    List<CorrectTreeLayoutData> findAllCorrectionForFamilyTree(@Param("familyTreeName") String familyTreeName);
}
