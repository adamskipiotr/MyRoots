package com.pada.myroots.domain.correctLayoutData;

import com.pada.myroots.client.Client;
import com.pada.myroots.domain.application.Application;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
class CorrectTreeLayoutService {

    private final CorrectTreeLayoutRepository correctTreeLayoutRepository;


    public void addCorrections(List<CorrectTreeLayoutDTO> correctTreeLayoutDTOs) {
        correctTreeLayoutDTOs.forEach(correctTreeLayoutDTO -> correctTreeLayoutRepository.save(new CorrectTreeLayoutData(correctTreeLayoutDTO.getPersonToMove(),
                correctTreeLayoutDTO.getDirection(), correctTreeLayoutDTO.getDirection(), correctTreeLayoutDTO.getMoveValue())));
    }

    public List<CorrectTreeLayoutData> getAllCorrectionsForFamilyTree(String familyTreeName) {
        return correctTreeLayoutRepository.findAllCorrectionForFamilyTree(familyTreeName);
    }

    public List<CorrectTreeLayoutData> getAllCorrectionsForFamilyTreeClient(Client client) {
        Application application = client.getApplication();
        String familyTreeName = application.getFamilyTreeName();
        return correctTreeLayoutRepository.findAllCorrectionForFamilyTree(familyTreeName);
    }

    public List<CorrectTreeLayoutData> getAllCorrectionsForFamilyTreeEmployee(String familyTreeName) {
        return correctTreeLayoutRepository.findAllCorrectionForFamilyTree(familyTreeName);
    }
}
