package com.pada.myroots.domain.familyMember.DTO;

import com.pada.myroots.domain.familyMember.FamilyMember;
import lombok.Data;


@Data
public class FamilyMemberDTO {

    private String name;
    private String lastName;
    private String familyTreeName;
    private String birthDate;
    private String deathDate;
    private String father;
    private String mother;
    private String spouse;
    private String isDescendant;
    private String applicationUUID;

    public FamilyMemberDTO(String name, String lastName, String birthDate, String deathDate) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }

    public FamilyMemberDTO(FamilyMember familyMember) {
        this.name = familyMember.getName();
        this.lastName = familyMember.getLastName();
        this.birthDate = familyMember.getBirthDate();
        this.deathDate = familyMember.getDeathDate();
    }
}
