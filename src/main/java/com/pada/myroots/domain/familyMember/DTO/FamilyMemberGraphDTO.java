package com.pada.myroots.domain.familyMember.DTO;

import lombok.Data;

@Data
public class FamilyMemberGraphDTO {
    private String child;
    private String parent;
    private String spouse;
    private String gender;
    private String lifetime;
    private String spouseLifetime;

    public FamilyMemberGraphDTO(String child, String parent, String spouse, String gender, String lifetime, String spouseLifetime) {
        this.child = child;
        this.parent = parent;
        this.spouse = spouse;
        this.gender = gender;
        this.lifetime = lifetime;
        this.spouseLifetime = spouseLifetime;
    }
}
