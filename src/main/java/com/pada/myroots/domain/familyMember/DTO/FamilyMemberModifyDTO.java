package com.pada.myroots.domain.familyMember.DTO;

import lombok.Data;


@Data
public class FamilyMemberModifyDTO {

    private String name;
    private String lastName;
    private String familyTreeName;
    private String birthDate;
    private String deathDate;
    private String person;

    public FamilyMemberModifyDTO(String name, String lastName, String birthDate, String deathDate) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }
}
