package com.pada.myroots.domain.familyMember;

import com.pada.myroots.domain.familyMember.DTO.FamilyMemberDTO;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberGraphDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
public class FamilyMember {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastName;
    private String birthDate;
    private String deathDate;
    private String familyTreeName;
    private String hasBloodTies;
    private UUID applicationUUID;

    @ManyToMany
    private Set<FamilyMember> children = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.ALL, CascadeType.ALL}, mappedBy = "children")
    private Set<FamilyMember> parents = new HashSet<>();

    @ManyToOne
    private FamilyMember spouse;

    @OneToMany(mappedBy = "spouse")
    private List<FamilyMember> spouses;

    FamilyMember(FamilyMemberDTO familyMemberDTO) {
        this.name = familyMemberDTO.getName();
        this.lastName = familyMemberDTO.getLastName();
        this.birthDate = familyMemberDTO.getBirthDate();
        this.deathDate = familyMemberDTO.getDeathDate();
        this.familyTreeName = familyMemberDTO.getFamilyTreeName();
        this.hasBloodTies = familyMemberDTO.getIsDescendant();
        this.applicationUUID = UUID.fromString(familyMemberDTO.getApplicationUUID());
    }

    public FamilyMemberGraphDTO graphDTO(String parentName, String spouseName, String spouseLifetime) {
        return new FamilyMemberGraphDTO(this.name + " " + lastName, parentName, spouseName, hasBloodTies, "(" + birthDate + "-" + deathDate + ")", spouseLifetime);
    }
}
