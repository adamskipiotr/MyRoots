package com.pada.myroots.domain.familyMember;


import com.pada.myroots.client.Client;
import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberDTO;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberModifyDTO;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberGraphWrapper;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;


@Component
@AllArgsConstructor
public class FamilyMemberFacade {

    private final FamilyMemberService familyMemberService;

    public void addNewFamilyMember(FamilyMemberDTO familyMemberDTO) {
        familyMemberService.addNewFamilyMember(familyMemberDTO);
    }

    public FamilyMemberWrapper getAllFamilyMembers(ClaimedApplicactionDto claimedApplicactionDto) {
        return familyMemberService.getAllFamilyMembers(claimedApplicactionDto);
    }

    public FamilyMemberGraphWrapper getAllFamilyMembersGraph(Client client) {
        return familyMemberService.getAllFamilyMembersGraph(client);
    }

    public FamilyMemberGraphWrapper getAllFamilyMembersGraphEmployee(UUID uuid) {
        return familyMemberService.getAllFamilyMembersGraphEmployee(uuid);
    }

    public void modifyFamilyMember(FamilyMemberModifyDTO familyMemberModifyDTO) {
        familyMemberService.modifyFamilyMember(familyMemberModifyDTO);
    }

    public FamilyMemberWrapper getAllFamilyMembersSingle(ClaimedApplicactionDto claimedApplicactionDto) {
        return familyMemberService.getAllFamilyMembersSingle(claimedApplicactionDto);
    }
}
