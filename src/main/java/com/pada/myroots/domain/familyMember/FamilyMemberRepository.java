package com.pada.myroots.domain.familyMember;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
interface FamilyMemberRepository extends JpaRepository<FamilyMember, Long> {


    @Query("select fa.id FROM FamilyMember fa where fa.name = :name and fa.applicationUUID = :applicationUUID")
    Long findByName(@Param("name") String name, @Param("applicationUUID") UUID applicationUUID);

    @Transactional
    @Modifying
    @Query("UPDATE FamilyMember fa SET fa.children = :child WHERE fa.id = :parentID")
    void addChildToParent(@Param("parentID") Long parentID, @Param("child") FamilyMember child);

    @Query("select fa FROM FamilyMember fa where fa.familyTreeName = :familyTreeName and fa.applicationUUID = :applicationUUID")
    List<FamilyMember> findAllByFamilyTreeName(@Param("familyTreeName") String familyTreeName, @Param("applicationUUID") UUID applicationUUID);

    @Query("select fa FROM FamilyMember fa where fa.id = :parent_id")
    List<FamilyMember> findParents(@Param("parent_id") Long parent_id);

    @Transactional
    @Modifying
    @Query("UPDATE FamilyMember fa SET fa.spouse = :spouseToAdd WHERE fa.id = :existingSpouse")
    void addSpouseToExisting(@Param("existingSpouse") Long existingSpouse, @Param("spouseToAdd") FamilyMember spouseToAdd);

    @Query("select fa FROM FamilyMember fa where fa.spouse = :spouse_id")
    List<FamilyMember> findSpouse(@Param("spouse_id") Long id);

    @Transactional
    @Modifying
    @Query("UPDATE FamilyMember fa SET fa.name = :name,fa.lastName = :lastName,fa.birthDate = :birthDate,fa.deathDate = :deathDate WHERE fa.id = :familyMemberToUpdateID")
    void updateExistingPerson(@Param("familyMemberToUpdateID") Long familyMemberToUpdateID, @Param("name") String name, @Param("lastName") String lastName,
                              @Param("birthDate") String birthDate, @Param("deathDate") String deathDate);

    @Query("select fa FROM FamilyMember fa where fa.name = :name")
    FamilyMember findByNameGetAllData(@Param("name") String name);

    @Query("select fa FROM FamilyMember fa where fa.familyTreeName = :familyTreeName and fa.applicationUUID = :applicationUUID and fa.spouse is null ")
    List<FamilyMember> findAllSingleByFamilyTreeName(@Param("familyTreeName") String familyTreeName, @Param("applicationUUID") UUID applicationUUID);
}
