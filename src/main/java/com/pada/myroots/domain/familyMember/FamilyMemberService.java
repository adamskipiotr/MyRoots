package com.pada.myroots.domain.familyMember;


import com.pada.myroots.client.Client;
import com.pada.myroots.domain.application.Application;
import com.pada.myroots.domain.application.ApplicationFacade;
import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberDTO;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberGraphDTO;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberModifyDTO;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberGraphWrapper;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberWrapper;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FamilyMemberService {

    private final FamilyMemberRepository familyMemberRepository;
    private final ApplicationFacade applicationFacade;
    private final EntityManager entityManager;


    @Transactional
    public void addNewFamilyMember(FamilyMemberDTO familyMemberDTO) {
        String fatherName = familyMemberDTO.getFather();
        UUID applicationUUID = UUID.fromString(familyMemberDTO.getApplicationUUID());
        Optional<Long> fatherIdOptional, motherIdOptional, spouseIdOptional;

        fatherIdOptional = this.checkIfRelatedIsDefined(fatherName, applicationUUID);
        String motherName = familyMemberDTO.getMother();
        motherIdOptional = this.checkIfRelatedIsDefined(motherName, applicationUUID);

        String spouseName = familyMemberDTO.getSpouse();
        spouseIdOptional = this.checkIfRelatedIsDefined(spouseName, applicationUUID);

        FamilyMember newFamilyMember = new FamilyMember(familyMemberDTO);

        if (spouseIdOptional.isPresent()) {
            Optional<FamilyMember> spouseFamilyMember = familyMemberRepository.findById(spouseIdOptional.get());
            spouseFamilyMember.ifPresent(newFamilyMember::setSpouse);
        }
        FamilyMember result = familyMemberRepository.save(newFamilyMember);
        Long childID = result.getId();
        fatherIdOptional.ifPresent(fatherID -> entityManager.createNativeQuery("INSERT INTO family_member_children (parents_id,children_id) VALUES (" + fatherID + "," + childID + ")").executeUpdate());
        motherIdOptional.ifPresent(motherID -> entityManager.createNativeQuery("INSERT INTO family_member_children (parents_id,children_id) VALUES (" + motherID + "," + childID + ")").executeUpdate());
        spouseIdOptional.ifPresent(spouseID -> familyMemberRepository.addSpouseToExisting(spouseID, result));
    }

    public FamilyMemberWrapper getAllFamilyMembers(ClaimedApplicactionDto claimedApplicactionDto) {
        UUID appUUID = UUID.fromString(claimedApplicactionDto.getApplicationUUID());
        List<FamilyMember> familyMemberList = familyMemberRepository.findAllByFamilyTreeName(claimedApplicactionDto.getFamilyTreeName(), appUUID);
        return getFamilyMemberWrapper(familyMemberList);
    }

    public FamilyMemberGraphWrapper getAllFamilyMembersGraph(Client client) {
        Application application = client.getApplication();
        String familyTreeName = application.getFamilyTreeName();
        UUID applicationUUID = application.getApplicationUUID();
        return getFamilyMemberGraphWrapper(applicationUUID, familyTreeName);
    }

    public void modifyFamilyMember(FamilyMemberModifyDTO familyMemberModifyDTO) {
        String personToModify = familyMemberModifyDTO.getPerson();
        personToModify = personToModify.substring(0, personToModify.indexOf(" "));
        FamilyMember familyMemberToUpdate = familyMemberRepository.findByNameGetAllData(personToModify);
        String nameUpdate = (!familyMemberModifyDTO.getName().equals("")) ? familyMemberModifyDTO.getName() : familyMemberToUpdate.getName();
        String lastNameUpdate = (!familyMemberModifyDTO.getName().equals("")) ? familyMemberModifyDTO.getLastName() : familyMemberToUpdate.getLastName();
        String birthDateUpdate = (!familyMemberModifyDTO.getName().equals("")) ? familyMemberModifyDTO.getBirthDate() : familyMemberToUpdate.getBirthDate();
        String deathDateUpdate = (!familyMemberModifyDTO.getName().equals("")) ? familyMemberModifyDTO.getDeathDate() : familyMemberToUpdate.getDeathDate();
        familyMemberRepository.updateExistingPerson(familyMemberToUpdate.getId(), nameUpdate, lastNameUpdate, birthDateUpdate, deathDateUpdate);
    }

    public FamilyMemberGraphWrapper getAllFamilyMembersGraphEmployee(UUID uuid) {
        String familyTreeName = applicationFacade.getFamilyTreeNameByUUID(uuid);
        return getFamilyMemberGraphWrapper(uuid, familyTreeName);
    }

    @NotNull
    @SuppressWarnings("unchecked")
    private FamilyMemberGraphWrapper getFamilyMemberGraphWrapper(UUID uuid, String familyTreeName) {
        List<FamilyMember> familyMemberList = familyMemberRepository.findAllByFamilyTreeName(familyTreeName, uuid);
        List<FamilyMemberGraphDTO> familyMemberDTOS = new LinkedList<>();
        for (FamilyMember familyMember : familyMemberList) {
            String parentName = "", spouseName = "", spouseLifetime = "";
            List<BigInteger> parents_id = entityManager.createNativeQuery("SELECT parents_id FROM family_member_children WHERE children_id =" + familyMember.getId()).getResultList();
            FamilyMember spouse = familyMember.getSpouse();
            if (spouse != null) {
                spouseName = spouse.getName() + " " + spouse.getLastName();
                spouseLifetime = "(" + spouse.getBirthDate() + "-" + spouse.getDeathDate() + ")";
            }
            for (BigInteger parent_id : parents_id) {
                Long parentIDLong = Long.valueOf(String.valueOf(parent_id));
                List<FamilyMember> parentData = familyMemberRepository.findParents(parentIDLong);
                if (parentData.get(0).getHasBloodTies().equals("Y")) {
                    parentName = parentData.get(0).getName() + " " + parentData.get(0).getLastName();
                }
            }
            familyMemberDTOS.add(familyMember.graphDTO(parentName, spouseName, spouseLifetime));
        }
        List<String> spouses = new LinkedList<>();
        for (FamilyMemberGraphDTO familyMemberGraphDTO : familyMemberDTOS) {
            String spouseName = familyMemberGraphDTO.getSpouse();
            if (!spouseName.equals(""))
                spouses.add(spouseName);
        }
        familyMemberDTOS.removeIf(familyMemberGraphDTO -> spouses.contains(familyMemberGraphDTO.getChild()) && familyMemberGraphDTO.getGender().equals("N"));

        return new FamilyMemberGraphWrapper(familyMemberDTOS, familyTreeName);
    }

    public FamilyMemberWrapper getAllFamilyMembersSingle(ClaimedApplicactionDto claimedApplicactionDto) {
        UUID appUUID = UUID.fromString(claimedApplicactionDto.getApplicationUUID());
        List<FamilyMember> familyMemberList = familyMemberRepository.findAllSingleByFamilyTreeName(claimedApplicactionDto.getFamilyTreeName(), appUUID);
        return getFamilyMemberWrapper(familyMemberList);
    }

    private Optional<Long> checkIfRelatedIsDefined(String relatedName, UUID applicationUUID) {
        Optional<Long> relatedPersonIdOptional = Optional.empty();
        if (!relatedName.equals("")) {
            relatedName = relatedName.substring(0, relatedName.indexOf(" "));
            relatedPersonIdOptional = Optional.of(familyMemberRepository.findByName(relatedName, applicationUUID));
        }
        return relatedPersonIdOptional;
    }

    @NotNull
    private FamilyMemberWrapper getFamilyMemberWrapper(List<FamilyMember> familyMemberList) {
        List<FamilyMemberDTO> familyMemberDTOS = familyMemberList
                .stream()
                .map(FamilyMemberDTO::new)
                .collect(Collectors.toList());
        FamilyMemberWrapper wrapper = new FamilyMemberWrapper();
        wrapper.setFamilyMemberList(familyMemberDTOS);
        return wrapper;
    }
}
