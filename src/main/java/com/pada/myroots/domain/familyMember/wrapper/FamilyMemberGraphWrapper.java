package com.pada.myroots.domain.familyMember.wrapper;

import com.pada.myroots.domain.familyMember.DTO.FamilyMemberGraphDTO;
import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class FamilyMemberGraphWrapper {
    @NonNull
    private final List<FamilyMemberGraphDTO> familyMemberList;
    @NonNull
    private final String familyTreeName;

}
