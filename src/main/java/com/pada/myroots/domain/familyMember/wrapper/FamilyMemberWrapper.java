package com.pada.myroots.domain.familyMember.wrapper;

import com.pada.myroots.domain.familyMember.DTO.FamilyMemberDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
public class FamilyMemberWrapper {
    List<FamilyMemberDTO> familyMemberList;

    public FamilyMemberWrapper() {
        familyMemberList = new LinkedList<>();
    }

    public void setFamilyMemberList(List<FamilyMemberDTO> familyMemberList) {
        this.familyMemberList = familyMemberList;
    }
}
