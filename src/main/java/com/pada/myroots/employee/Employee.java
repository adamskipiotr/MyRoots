package com.pada.myroots.employee;

import com.pada.myroots.domain.application.Application;
import com.pada.myroots.employee.dto.EmployeeDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
    private boolean isActive;
    private String roles;

    @OneToMany(mappedBy = "employee",   //  'mappedBy = "recruit"' oznacza,że 'private Recruit recruit' w
            cascade = CascadeType.ALL, // klasie Education odpowiada za relację (zawiera klucz obcy do query by znaleźć wszystkie wizyty dla danego rekruta
            orphanRemoval = true)
    private List<Application> handledApplications;

    public void setValuesFromDTO(EmployeeDTO dto, String password) {
        this.name = dto.getName();
        this.lastName = dto.getLastName();
        this.login = dto.getLogin();
        this.password = password;
        this.email = dto.getEmail();
        this.isActive = true;
        this.roles = "ROLE_EMPLOYEE";
    }
}
