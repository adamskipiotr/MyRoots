package com.pada.myroots.employee;

import com.pada.myroots.domain.application.DTO.ApplicationDto;
import com.pada.myroots.domain.application.ApplicationFacade;
import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.domain.correctLayoutData.CorrectTreeLayoutDTO;
import com.pada.myroots.domain.correctLayoutData.CorrectTreeLayoutData;
import com.pada.myroots.domain.correctLayoutData.CorrectTreeLayoutFacade;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberDTO;
import com.pada.myroots.domain.familyMember.FamilyMemberFacade;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberModifyDTO;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberGraphWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@CrossOrigin("*")
@Controller
@RequestMapping("employee")
@AllArgsConstructor
public class EmployeeController {

    private final EmployeeFacade employeeFacade;
    private final ApplicationFacade applicationFacade;
    private final FamilyMemberFacade familyMemberFacade;
    private final CorrectTreeLayoutFacade correctTreeLayoutFacade;

    @GetMapping("/checkRequests")
    @ResponseBody
    public List<ApplicationDto> getApplicationRequest() {
        return applicationFacade.getAllUnhandledApplications();
    }

    @GetMapping("/getClaimedRequests")
    @ResponseBody
    public List<ClaimedApplicactionDto> getClaimedRequests(Principal principal) {
        String employeeName = principal.getName();
        Employee assignedEmployee = employeeFacade.findByLogin(employeeName);
        return applicationFacade.getClaimedRequests(assignedEmployee);
    }

    @GetMapping("/claimRequest/{UUID}")
    @ResponseBody
    public void claimApplicationRequest(@PathVariable(name = "UUID") String UUID, Principal principal) {
        String employeeName = principal.getName();
        Employee assignedEmployee = employeeFacade.findByLogin(employeeName);
        applicationFacade.assignToApplication(UUID, assignedEmployee);
    }

    @PostMapping("/correctTreeLayout")
    @ResponseBody
    public void correctTreeLayout(@RequestBody List<CorrectTreeLayoutDTO> correctTreeLayoutDTOs) {
        correctTreeLayoutFacade.addCorrections(correctTreeLayoutDTOs);
    }

    @PostMapping("/getCorrections/{familyTreeName}")
    @ResponseBody
    public List<CorrectTreeLayoutData> getAllCorrectionsForTree(@PathVariable(name = "familyTreeName") String familyTreeName) {
        return correctTreeLayoutFacade.getAllCorrectionsForFamilyTree(familyTreeName);
    }

    @GetMapping("/getFamilyTree/{UUID}")
    @ResponseBody
    public FamilyMemberGraphWrapper getFamilyTree(@PathVariable(name = "UUID") UUID uuid) {
        return familyMemberFacade.getAllFamilyMembersGraphEmployee(uuid);
    }

    @GetMapping("/finishTree/{familyTreeName}")
    @ResponseBody
    public void finishCreatingTree(@PathVariable(name = "familyTreeName") String familyTreeName) {
        applicationFacade.finishCreatingTree(familyTreeName);
    }

    @PostMapping("/addFamilyMember")
    @ResponseBody
    public void addFamilyMember(@RequestBody FamilyMemberDTO familyMemberDTO) {
        familyMemberFacade.addNewFamilyMember(familyMemberDTO);
    }

    @PostMapping("/modifyFamilyMember")
    @ResponseBody
    public void modifyFamilyMember(@RequestBody FamilyMemberModifyDTO familyMemberModifyDTO) {
        familyMemberFacade.modifyFamilyMember(familyMemberModifyDTO);
    }

    @GetMapping("/getTreeLayoutCorrections/{familyTreeName}")
    @ResponseBody
    public List<CorrectTreeLayoutData> getTreeLayoutCorrections(@PathVariable(name = "familyTreeName") String familyTreeName) {
        return correctTreeLayoutFacade.getAllCorrectionsForFamilyTreeEmployee(familyTreeName);
    }
}
