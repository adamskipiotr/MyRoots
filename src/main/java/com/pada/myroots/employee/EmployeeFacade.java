package com.pada.myroots.employee;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class EmployeeFacade {
    private final EmployeeService employeeService;

    public Employee findByLogin(String login) {
        return employeeService.findByLogin(login);
    }

    public void addNewEmployee(Employee newEmployee) {
        employeeService.addNewEmployee(newEmployee);
    }

    public List<Employee> getAllEmployeesStats() {
        return employeeService.getAllEmployeesStats();
    }

    public Optional<Employee> findByLoginAuth(String login) {
        return employeeService.findByLoginAuth(login);
    }

    public void deactivateEmployeeAccount(Long employeeID) {
        employeeService.deactivateEmployeeAccount(employeeID);
    }

    public void deleteEmployeeAccount(Long employeeID) {
        employeeService.deleteEmployeeAccount(employeeID);
    }
}
