package com.pada.myroots.employee;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public Employee findByLogin(String login) {
        return employeeRepository.findByLogin(login).orElse(null);
    }

    public void addNewEmployee(Employee newEmployee) {
        employeeRepository.save(newEmployee);
    }

    public List<Employee> getAllEmployeesStats() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> findByLoginAuth(String login) {
        return employeeRepository.findByLogin(login);
    }

    public void deactivateEmployeeAccount(Long employeeID) {
        employeeRepository.deactivateEmployeeAccount(employeeID);
    }

    public void deleteEmployeeAccount(Long employeeID) {
        Optional<Employee> employeeOptional = employeeRepository.findById(employeeID);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            employeeRepository.delete(employee);
        }
    }
}
