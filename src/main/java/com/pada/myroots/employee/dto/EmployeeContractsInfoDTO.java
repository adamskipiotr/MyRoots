package com.pada.myroots.employee.dto;

import com.pada.myroots.employee.Employee;
import lombok.Data;
import lombok.NonNull;

@Data
public class EmployeeContractsInfoDTO {

    @NonNull
    private Long employeeID;
    @NonNull
    private String employeeName;
    @NonNull
    private String activeContracts;
    @NonNull
    private String providedInformations;

    public EmployeeContractsInfoDTO(Employee employee, String activeContractsInfo, String additionalInfo) {
        employeeID = employee.getId();
        employeeName = employee.getName();
        activeContracts = activeContractsInfo;
        providedInformations = additionalInfo;
    }
}
