package com.pada.myroots.employee.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EmployeeDTO {

    private String name;
    private String lastName;
    private String login;
    private String password;
    private String email;
}
