package com.pada.myroots.security;


import com.pada.myroots.admin.Admin;
import com.pada.myroots.admin.AdminFacade;
import com.pada.myroots.client.Client;
import com.pada.myroots.client.ClientFacade;

import com.pada.myroots.employee.Employee;
import com.pada.myroots.employee.EmployeeFacade;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final ClientFacade clientFacade;
    private final EmployeeFacade employeeFacade;
    private final AdminFacade adminFacade;


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        Optional<Client> user = clientFacade.findByLoginAuth(login);
        if (!user.isPresent()) {
            Optional<Employee> headUser = Optional.empty();
            try {
                headUser = employeeFacade.findByLoginAuth(login);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!headUser.isPresent()) {
                Optional<Admin> admin = adminFacade.findByLoginAuth(login);
                return admin.map(CustomUserDetails::new).get();
            }
            return headUser.map(CustomUserDetails::new).get();
        }


        return user.map(CustomUserDetails::new).get();

    }
}
