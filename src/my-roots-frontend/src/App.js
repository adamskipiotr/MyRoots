import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import AdminAuthenticatedRoute from './Services/LoginService/AdminAuthenticatedRoute.js'
import EmployeeAuthenticatedRoute from './Services/LoginService/EmployeeAuthenticatedService.js';
import ClientAuthenticatedRoute from './Services/LoginService/ClientAuthenticaredRoute.js'
import MainPage from './default/MainPage';
import LoginPage from './default/LoginPage';
import RegisterPage from './default/RegisterPage';
import ClientPage from './client/ClientPage';
import AdminPage from './admin/AdminPage';
import EmployeePage from './employee/EmployeePage';
import './App.css';
import history from './common/history';
import ExampleFamilyTree from './default/ExampleFamilyTree';
import ClientFamilyTree from './client/ClientFamilyTree';
import EmployeeFamilyTree from './employee/EmployeeFamilyTree';

class App extends React.Component {

    render() {
        return (
            < Router history={history}>
                <Switch>
                    <Route path="/main" component={MainPage}/>
                    <ClientAuthenticatedRoute path="/client" component={ClientPage}/>
                    <ClientAuthenticatedRoute path="/clientTree" component={ClientFamilyTree}/>
                    <EmployeeAuthenticatedRoute path="/employee" component={EmployeePage}/>
                    <AdminAuthenticatedRoute path="/admin" component={AdminPage}/>
                    <Route path="/login" component={LoginPage}/>
                    <Route path="/register" component={RegisterPage}/>
                    <Route path="/example" component={ExampleFamilyTree}/>
                    <EmployeeAuthenticatedRoute path="/employeeTree" component={EmployeeFamilyTree}/>
                    <Route path="/" component={MainPage}/>
                </Switch>
            </Router>
        );
    }
}

export default App;