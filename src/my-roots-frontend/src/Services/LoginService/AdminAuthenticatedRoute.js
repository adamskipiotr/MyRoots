import React from 'react';
import AuthenticationService from './AuthenticationService.js';
import {Route, Redirect, withRouter} from 'react-router-dom';

class AdminAuthenticatedRoute extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (AuthenticationService.isAdminLoggedIn()) {
            return <Route {...this.props} />;
        } else if (AuthenticationService.isEmployeeLoggedIn()) {
            return <Redirect to="/employee"/>;
        } else if (AuthenticationService.isClientLoggedIn()) {
            return <Redirect to="/client"/>;
        } else {
            return <Redirect to="/main"/>;
        }
    }
}

export default withRouter(AdminAuthenticatedRoute);