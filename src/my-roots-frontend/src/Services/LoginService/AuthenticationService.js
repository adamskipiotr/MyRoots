import axios from 'axios';

const client = 'authUserAsClient';
const employee = 'authUserAsEmployee';
const admin = 'authUserAsAdmin';
const API_URL = 'http://localhost:8080'


class AuthenticationService {

    executeBasicAuthentication(username, password) {
        return axios.get(`${API_URL}/login`, {
            headers: {
                authorization: this.createBasicAuthToken(username, password),
            },
        });
    }

    createBasicAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ':' + password);
    }

    registerSuccessfulClientLogin(username, password) {
        sessionStorage.setItem(client, username);
        var token = this.createBasicAuthToken(username, password);
        sessionStorage.setItem("TOKEN_CLIENT", token)
        this.setupAxiosInterceptors(token);
    }

    registerSuccessfulEmployeeLogin(username, password) {
        sessionStorage.setItem(employee, username);
        var token = this.createBasicAuthToken(username, password);
        sessionStorage.setItem("TOKEN_EMPLOYEE", token)
        this.setupAxiosInterceptors(token);
    }

    registerSuccessfulAdminLogin(username, password) {
        sessionStorage.setItem(admin, username);
        var token = this.createBasicAuthToken(username, password);
        sessionStorage.setItem("TOKEN_ADMIN", token)
        this.setupAxiosInterceptors(token);
    }

    setupAxiosInterceptors(token) {
        axios.interceptors.request.use(config => {
            if (this.isClientLoggedIn() || this.isEmployeeLoggedIn() || this.isAdminLoggedIn()) {
                config.headers.authorization = token;
            }
            return config;
        })
    }

    isClientLoggedIn() {
        let user = sessionStorage.getItem(client);
        if (user === null) return false;
        var token = sessionStorage.getItem("TOKEN_CLIENT")
        axios.interceptors.request.use(config => {
            if (true) {
                config.headers.authorization = token;
            }
            return config;
        })
        return true;
    }

    logoutClient() {
        sessionStorage.clear();
        localStorage.clear();
    }

    getClientName() {
        return sessionStorage.getItem(client);
    }

    isEmployeeLoggedIn() {
        let user = sessionStorage.getItem(employee);
        if (user === null) return false;
        var token = sessionStorage.getItem("TOKEN_EMPLOYEE")
        axios.interceptors.request.use(config => {
            if (true) {
                config.headers.authorization = token;
            }
            return config;
        })
        return true;
    }

    logoutEmployee() {
        sessionStorage.clear();
    }

    getEmployeeName() {
        return sessionStorage.getItem(employee);
    }

    isAdminLoggedIn() {
        let user = sessionStorage.getItem(admin);
        if (user === null) return false;
        var token = sessionStorage.getItem("TOKEN_ADMIN")
        axios.interceptors.request.use(config => {
            if (true) {
                config.headers.authorization = token;
            }
            return config;
        })
        return true;
    }

    logoutAdmin() {
        sessionStorage.clear();
    }

    getAdminName() {
        return sessionStorage.getItem(admin);
    }
}

export default new AuthenticationService();