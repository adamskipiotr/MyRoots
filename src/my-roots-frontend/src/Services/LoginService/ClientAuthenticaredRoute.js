import React from 'react';
import AuthenticationService from './AuthenticationService.js';
import {Route, Redirect, withRouter} from 'react-router-dom';

class ClientAuthenticatedRoute extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (AuthenticationService.isClientLoggedIn()) {
            return <Route {...this.props} />;
        } else if (AuthenticationService.isEmployeeLoggedIn()) {
            return <Redirect to="/employee"/>;
        } else if (AuthenticationService.isAdminLoggedIn()) {
            return <Redirect to="/admin"/>;
        } else {
            return <Redirect to="/main"/>;
        }
    }
}

export default withRouter(ClientAuthenticatedRoute);