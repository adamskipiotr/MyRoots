import React from 'react';
import AuthenticationService from './AuthenticationService.js';
import {Route, Redirect, withRouter} from 'react-router-dom';

class EmployeeAuthenticatedRoute extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (AuthenticationService.isEmployeeLoggedIn()) {
            return <Route {...this.props} />;
        } else if (AuthenticationService.isClientLoggedIn()) {
            return <Redirect to="/client"/>;
        } else if (AuthenticationService.isAdminLoggedIn()) {
            return <Redirect to="/admin"/>;
        } else {
            return <Redirect to="/main"/>;
        }
    }
}

export default withRouter(EmployeeAuthenticatedRoute);