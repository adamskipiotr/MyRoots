import * as d3 from 'd3'
import '../common/TreeGraph.css';
import "../common/Common.css"

class TreeService {

    addNames(svg, information) {
        var names = svg.append("g").selectAll("text")
            .data(information.descendants());
        names.enter().append("text")
            .text(function (d) {
                return d.data.child;
            })
            .attr("x", function (d) {
                return 3.5 * d.data.child.length + (2 * d.x / 2 - 10 * d.data.child.length)
            })
            .attr("y", function (d) {
                return d.y;
            })
            .classed("bigger", true);
        names.enter().append("text")
            .text(function (d) {
                return d.data.lifetime;
            })
            .attr("x", function (d) {
                return 3.5 * d.data.child.length + (2 * d.x / 2 - 10 * d.data.child.length)
            })
            .attr("y", function (d) {
                return d.y + 16;
            })
            .classed("smaller", true);
    }

    addRectangles(svg, information) {
        var rectangles = svg.append("g").selectAll("rect")
            .data(information.descendants());
        rectangles.enter().append("rect")
            .attr("width", function (d) {
                return 13 * d.data.child.length + "px"
            })
            .attr("x", function (d) {
                return 2 * d.x / 2 - this.width.baseVal.value;
            })
            .attr("y", function (d) {
                return d.y - 20;
            });
    }

    addSpouseRectangles(svg, information) {
        var spouseRectangles = svg.append("g").selectAll("rect")
            .data(information.descendants());
        spouseRectangles.enter().append("rect")
            .attr("width", function (d) {
                if (d.data.spouse !== undefined) {
                    return 13 * d.data.spouse.length + "px"
                } else
                    return 0;
            })
            .attr("x", function (d) {
                if (d.data.spouse !== undefined)
                    return 10 * d.data.spouse.length + (2 * d.x / 2 - 10 * d.data.spouse.length) + 71;
                return d.x + 60;
            })
            .attr("y", function (d) {
                return d.y - 20;
            })
            .classed("hide", function (d) {
                if (d.data.spouse === undefined)
                    return true;
                else
                    return false;
            });
    }

    addSpouseNames(svg, information) {
        var spouseNames = svg.append("g").selectAll("text")
            .data(information.descendants());
        spouseNames.enter().append("text")
            .text(function (d) {
                return d.data.spouse;
            })
            .attr("x", function (d) {
                if (d.data.spouse !== undefined) {
                    return 11 * d.data.spouse.length + (2 * d.x / 2 - 5 * d.data.spouse.length) + 71;
                }
            })
            .attr("y", function (d) {
                return d.y;
            })
            .classed("bigger", true)
        spouseNames.enter().append("text")
            .text(function (d) {
                return d.data.spouseLifetime;
            })
            .attr("x", function (d) {
                if (d.data.spouse !== undefined) {
                    return 11 * d.data.spouse.length + (2 * d.x / 2 - 5 * d.data.spouse.length) + 71;
                }
            })
            .attr("y", function (d) {
                return d.y + 16;
            })
            .classed("smaller", true);
    }

    addConnections2(svg, information) {
        var connections2 = svg.append("g").selectAll("path")
            .data(information.links());
        connections2.enter().append("path")
            .attr("d", function (d) {
                var pathBegin = 10 * d.source.data.child.length + (2 * d.source.x / 2 - 10 * d.source.data.child.length) + 35
                return "m" + pathBegin + "," + d.source.y + "h 35";
            });
    }

    addConnections3(svg, information) {
        var connections2 = svg.append("g").selectAll("path")
            .data(information.links());
        connections2.enter().append("path")
            .attr("d", function (d) {
                if (d.target.data.spouse === undefined || d.target.data.spouse === "") {
                    return 0;
                } else {
                    var pathBegin = d.target.x
                    var pathEnd = 70
                    return "m" + pathBegin + "," + d.target.y + "h " + pathEnd;
                }
            });
    }

    addConnections1(svg, information) {
        var connections1 = svg.append("g").selectAll("path")
            .data(information.links());
        connections1.enter().append("path")
            .attr("d", function (d) {
                var targetPosition = d.target.x - 5 * d.target.data.child.length;
                var pathBegin = 10 * d.source.data.child.length + (2 * d.source.x / 2 - 10 * d.source.data.child.length)
                return "m" + pathBegin + "," + d.source.y + "h 35 v 50 H" + targetPosition + " V" + (d.target.y - 20);
            });
    }

    findMaxXCoordinate(dataStructure, array) {
        var maxXValue = 0;
        this.svg = d3.select("p").attr("class", "graph-svg-component").append("svg")
            .attr("width", 3.0 * window.screen.width).attr("height", (dataStructure.height) * 100)
            .append("g").attr("transform", "translate(90,50)");
        var mockSeparations = [];
        this.correctPositions(dataStructure, mockSeparations)
        this.addConnections1(this.svg, dataStructure)
        this.addConnections2(this.svg, dataStructure)
        this.addConnections3(this.svg, dataStructure)
        this.addRectangles(this.svg, dataStructure)
        this.addSpouseRectangles(this.svg, dataStructure)
        this.addNames(this.svg, dataStructure)
        this.addSpouseNames(this.svg, dataStructure)

        for (var i = 0; i < dataStructure.descendants().length; i++) {
            var desc = dataStructure.descendants()[i]
            if (desc.x > maxXValue) {
                maxXValue = desc.x;
            }
        }

        d3.select("svg").remove();
        return maxXValue;
    }

    correctPositions(information, separationArray) {
        var shift = 8;
        for (var i = 0; i < information.descendants().length; i++) {
            var desc = information.descendants()[i]

            for (const separation of separationArray) {
                console.log("aaa")
                if (separation.personToMove === desc.data.child) {
                    if (separation.direction === "prawo") {
                        console.log(desc.x)
                        desc.x += parseInt(separation.moveValue);
                        console.log(desc.x)

                    } else if (separation.direction === "lewo") {
                        desc.x -= parseInt(separation.moveValue);
                    } else if (separation.direction === "gora") {
                        desc.y -= parseInt(separation.moveValue);
                    } else if (separation.direction === "dol") {
                        desc.y += parseInt(separation.moveValue);
                    }
                }
            }

            if (desc.depth > 0 && desc.data.spouse !== undefined) {
                if (desc.data.spouse.length > 5) {
                    if (desc.data.spouse.length > 10) {
                        shift = 7
                    }
                    for (var j = i + 1; j < information.descendants().length; j++) {
                        var descTwo = information.descendants()[j]
                        if (descTwo.x > desc.x) {
                            if (descTwo.data.child.length > 10) {
                                shift += 1
                            }
                            descTwo.x += shift * desc.data.spouse.length
                        }
                    }
                }
            }
        }
    }
}

export default new TreeService();