import axios from "axios";


class AdminApi {

    getAllContracts() {
        return axios({
            method: "get",
            url: `http://localhost:8080/admin/getContracts`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    deactivateEmployeeAccount(employeeID) {
        return axios({
            method: "get",
            url: `http://localhost:8080/admin/deactivate/` + employeeID,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    deleteEmployeeAccount(employeeID) {
        return axios({
            method: "get",
            url: `http://localhost:8080/admin/delete/` + employeeID,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }
}

export default new AdminApi();