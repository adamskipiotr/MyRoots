import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css"
import {Button} from "reactstrap";
import NewEmployee from "./NewEmployee"
import ContractsList from "../contracts/ContractsList"
import AdminAPI from "./AdminAPI"
import EmployeesContracts from "./EmployeesContracts"
import AuthenticationService from '../Services/LoginService/AuthenticationService'


class AdminPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            contracts: [],
            status: "wrealizacji",
            addNewEmployeePanel: false,
            checkContractsButtonActive: false,
        }
        this.showAddNewEmployee = this.showAddNewEmployee.bind(this);
        this.checkEmployees = this.checkEmployees.bind(this);
        this.logoutAdmin = this.logoutAdmin.bind(this);
        this.fetchContractsInfo = this.fetchContractsInfo.bind(this);
    }


    showAddNewEmployee() {
        this.setState({
            addNewEmployeePanel: !this.state.addNewEmployeePanel
        });
        this.setState(({
            checkContractsButtonActive: false
        }))
    }

    checkEmployees(event) {
        event.preventDefault();
        this.setState(({
            addNewEmployeePanel: false
        }));
        this.setState({
            checkContractsButtonActive: !this.state.checkContractsButtonActive
        }, () => {
            this.fetchContractsInfo()
        });
    }

    fetchContractsInfo() {
        if (this.state.checkContractsButtonActive) {
            AdminAPI.getAllContracts()
                .then(response => {
                    this.setState({contracts: response.data})
                })
        }
    }

    logoutAdmin(event) {
        event.preventDefault();
        AuthenticationService.logoutAdmin();
        window.location.href = '/main'
    }

    render() {
        return (
            <>
                <div className=" large">MY ROOTS - STRONA ADMINISTRATORA</div>
                <div className="info-background-nude">
                    <div className="button-wrapper-emp">
                        <Button color="secondary" size="lg" onClick={this.checkEmployees}>Zobacz statystyki
                            pracowników</Button>
                        <div className="divider"/>
                        <Button color="secondary" size="lg" onClick={this.showAddNewEmployee}>Dodaj nowego
                            pracownika</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.logoutAdmin}>Wyloguj</Button>
                    </div>
                    <div className="info-background-transparent">
                        {this.state.checkContractsButtonActive && <EmployeesContracts/>}
                        {this.state.checkContractsButtonActive && <ContractsList contracts={this.state.contracts}/>}
                    </div>
                    <div className="info-background-transparent">
                        {this.state.addNewEmployeePanel && <NewEmployee/>}
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(AdminPage);