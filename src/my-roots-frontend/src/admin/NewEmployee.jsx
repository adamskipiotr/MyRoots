import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css"
import {Button} from "reactstrap";
import {Input} from 'reactstrap';
import {Col} from 'react-bootstrap';
import axios from "axios";


class NewEmployee extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            employee: {
                name: "",
                lastName: "",
                email: "",
                login: "",
                password: "",
                passwordRepeat: "",
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleRegisterClick = this.handleRegisterClick.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    changeHandler = event => {
        event.persist();

        let value = event.target.value;

        this.setState(prevState => ({
            employee: {...prevState.employee, [event.target.name]: value}
        }))
    };


    handleRegisterClick(event) {
        event.preventDefault();
        return axios({

            method: "post",
            url: `http://localhost:8080/admin/addEmployee`,
            data: this.state.employee,
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.status === 200) {
                    alert("Pracownik został dodany")
                }
            });
    }

    render() {
        return (
            <>
                <div className="info-background-transparent">
                    <h3>Zarejestruj nowego pracownika</h3>
                    <div className="button-wrapper-block">
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Imię:
                                <Input name="name" id="nameInput"
                                       type="text" placeholder="Imię"
                                       value={this.state.employee.name}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Nazwisko:
                                <Input name="lastName" id="lastnameInput"
                                       type="text" placeholder="Nazwisko"
                                       value={this.state.employee.lastName}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Email:
                                <Input name="email" id="emailInput"
                                       type="email" placeholder="Email"
                                       value={this.state.employee.email}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Login:
                                <Input name="login" id="loginInput"
                                       type="text" placeholder="Login"
                                       value={this.state.employee.login}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Hasło:
                                <Input name="password" id="passwordInput"
                                       type="password" placeholder="Hasło"
                                       value={this.state.employee.password}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Powtórz hasło:
                                <Input name="passwordRepeat" id="passwordRepeatInput"
                                       type="password" placeholder="Powtórz hasło"
                                       value={this.state.employee.passwordRepeat}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </div>
                    <div className="button-wrapper-center">
                        <Button color="secondary" size="lg"
                                onClick={() => this.props.history.push('/admin')}>Powrót</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.handleRegisterClick}>Zarejestruj</Button>
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(NewEmployee);