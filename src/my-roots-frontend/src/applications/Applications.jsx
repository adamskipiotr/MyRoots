import React from 'react';
import {Button} from 'reactstrap';
import EmployeeAPI from '../employee/EmployeeAPI'


class Application extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            applicationID: this.props.applicationUUID,
        }
        this.claimApplication = this.claimApplication.bind(this);
    }

    claimApplication() {
        EmployeeAPI.claimApplication(this.state.applicationID)
    }

    render() {
        return (
            <li className="item">
                <div className="content">
                    <div><b> {this.props.familyTreeName}</b> {this.props.status} {this.props.provided}</div>
                    <Button disabled={this.props.status !== "nierozpoczety"} className="sm" color="secondary"
                            onClick={this.claimApplication}> Przejmij</Button>
                </div>
            </li>
        )
    }
}

export default Application;