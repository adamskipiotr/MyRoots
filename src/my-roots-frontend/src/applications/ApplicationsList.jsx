import React from 'react';
import Application from './Applications';


class ApplicationsList extends React.Component {

    applicationToApplicationItem = application => {
        const applicationUUID = application.applicationUUID;
        const status = application.status;
        const familyTreeName = application.familyTreeName
        const providedInformations = application.providedInformations;

        return <Application key={applicationUUID} applicationUUID={applicationUUID} familyTreeName={familyTreeName}
                            status={status} provided={providedInformations}/>;
    }

    render() {
        return (
            <ul className="ui relaxed divided list selection">
                {this.props.applications.map(this.applicationToApplicationItem)}
            </ul>
        )
    }
}

export default ApplicationsList;