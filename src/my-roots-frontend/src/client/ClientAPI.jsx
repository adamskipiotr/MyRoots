import axios from "axios";


class ClientAPI {

    getClientFamilyTree() {
        return axios({
            method: "get",
            url: "http://localhost:8080/client/getFamilyTree"
        })
    }

    getRequestStatus() {
        return axios({
            method: "get",
            url: `http://localhost:8080/client/getRequestStatus`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }

    createTreeNewRequest() {
        return axios({
            method: "get",
            url: `http://localhost:8080/client/createRequest`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }

    getTreeName() {
        return axios({
            method: "get",
            url: `http://localhost:8080/client/getTreeName`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }

    getTreeLayoutCorrections() {
        return axios({
            method: "get",
            url: `http://localhost:8080/client/getTreeLayoutCorrections`,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}

export default new ClientAPI();