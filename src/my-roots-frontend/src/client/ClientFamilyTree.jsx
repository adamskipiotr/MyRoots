import React from 'react';
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import 'react-tree-graph/dist/style.css'
import * as d3 from 'd3'
import '../common/TreeGraph.css'
import {Button} from "reactstrap";
import {saveSvgAsPng} from "save-svg-as-png"
import ClientAPI from './ClientAPI'
import TreeService from '../Services/TreeService'


class ClientFamilyTree extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isRendered: false,
            enableDownload: false,
            familyTreeName: "",
            data: [],
            separationArray: [],
            svg: ""
        }
        this.handleButtonTree = this.handleButtonTree.bind(this);
        this.handleDownloadButton = this.handleDownloadButton.bind(this);
        this.getTreeLayoutCorrections = this.getTreeLayoutCorrections.bind(this);
    }

    componentWillUnmount = () => {
        d3.select("svg").remove();
    }

    componentWillMount = () => {
        ClientAPI.getClientFamilyTree()
            .then(response => {
                this.setState({data: response.data.familyMemberList}, () => {
                    this.getTreeLayoutCorrections()
                })
            })
    }

    getTreeLayoutCorrections() {
        ClientAPI.getTreeLayoutCorrections()
            .then(response => {
                this.setState({separationArray: response.data}, () => {
                    this.handleButtonTree()
                })
            })
    }

    handleDownloadButton = () => {
        ClientAPI.getTreeName().then(response => {
            var filename = response.data + " - drzewo genealogiczne"
            saveSvgAsPng(document.getElementsByTagName("svg")[0], filename + ".png", {
                scale: 1,
                backgroundColor: "#FFFFFF"
            });
        })

    }

    handleButtonTree = () => {

        this.setState(state => ({
            enableDownload: !state.enableDownload
        }));
        if (this.state.isRendered === false) {
            this.setState(({
                isRendered: true,
                buttonColor: "secondary",
                downloadButtonColor: "success"
            }));
            var dataStructure = d3.stratify()
                .id(function (d) {
                    return d.child;
                })
                .parentId(function (d) {
                    return d.parent;
                })(this.state.data);
            var treeStructure = d3.tree().size([2 * window.screen.width, 1]);
            var information = treeStructure(dataStructure);
            var height = dataStructure.height

            treeStructure = d3.tree().size([2 * window.screen.width, height * 100]);
            var informationTestLayout = treeStructure(dataStructure);

            var maxX = TreeService.findMaxXCoordinate(informationTestLayout, this.state.separationArray);
            this.svg = d3.select("p").attr("class", "graph-svg-component").append("svg")
                .attr("width", maxX + 600).attr("height", (height + 1) * 100)
                .append("g").attr("transform", "translate(90,50)");

            treeStructure = d3.tree().size([2 * window.screen.width, height * 100]);
            information = treeStructure(dataStructure);

            TreeService.correctPositions(information, this.state.separationArray)

            TreeService.addConnections1(this.svg, information)
            TreeService.addConnections2(this.svg, information)
            TreeService.addConnections3(this.svg, information)
            TreeService.addRectangles(this.svg, information)
            TreeService.addSpouseRectangles(this.svg, information)
            TreeService.addNames(this.svg, information)
            TreeService.addSpouseNames(this.svg, information)
        } else {
            d3.select("svg").remove();
            this.setState(({
                isRendered: false
            }));
        }
    }

    render() {
        return (
            <>
                <div className="info-background-nude">
                    <div className="button-wrapper-inline">
                        <Button color="secondary" size="lg" onClick={() => this.props.history.push('/client')}>Powrót na
                            stronę klienta</Button>
                        <div className="divider"/>
                        <Button color="secondary" size="lg" onClick={this.handleButtonTree}>Pokaż</Button>
                        <div className="divider"/>
                        <Button color="secondary" size="lg" disabled={this.state.enableDownload === false}
                                onClick={this.handleDownloadButton}>Pobierz</Button>
                    </div>
                    <p></p>
                </div>
            </>
        )
    }
}

export default withRouter(ClientFamilyTree);
