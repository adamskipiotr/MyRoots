import React from "react";
import {Route, Redirect, withRouter} from "react-router-dom";
import "../common/Common.css"
import {Button} from "reactstrap";
import ClientAPI from "./ClientAPI";
import AuthenticationService from '../Services/LoginService/AuthenticationService';


class ClientPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            status: "niewyslany",
        };
        this.sendRequest = this.sendRequest.bind(this);
        this.logoutClient = this.logoutClient.bind(this);
    }


    sendRequest(event) {
        event.preventDefault();
        ClientAPI.createTreeNewRequest()
            .then(Response => {
                if (Response.data === "success") {
                    this.setState(({
                        status: "nierozpoczety"
                    }));
                    this.showStatus()
                }
            })
        alert("Prośba została wysłana")
    }

    componentWillMount() {
        ClientAPI.getRequestStatus()
            .then(Response => {
                this.setState(({
                    status: Response.data
                }));
            })
    }

    logoutClient(event) {
        event.preventDefault();
        AuthenticationService.logoutClient();
        window.location.href = '/main'
    }

    render() {
        return (
            <>
                <div className=" large">MY ROOTS - POZNAJ HISTORIĘ SWOJEJ RODZINY</div>
                <div className="info-background-nude">
                    <h3>Status twojego wniosku: {this.showStatus()} </h3>
                    <div className="button-wrapper-emp">
                        <Button disabled={this.state.status !== "niewyslany"} color="secondary" size="lg"
                                onClick={this.sendRequest}>Wyślij wniosek</Button>
                        <div className="divider"/>
                        <Button disabled={this.state.status !== "zakonczony"} color="secondary" size="lg"
                                onClick={() => this.props.history.push('/clientTree')}>Pobierz drzewo
                            genealogiczne</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.logoutClient}>Wyloguj</Button>
                    </div>
                </div>
            </>
        );
    }

    showStatus() {
        if (this.state.status === "zakonczony") {
            return <span style={{color: 'green'}}>Zakończony</span>;
        } else if (this.state.status === "wrealizacji") {
            return <span style={{color: 'orange'}}>W trakcie realizacji</span>
        } else if (this.state.status === "niewyslany") {
            return <span style={{color: 'red'}}>Nie wysłałeś zgłoszenia</span>
        } else {
            return <span style={{color: 'red'}}>Nierozpoczęty</span>;
        }
    }
}

export default withRouter(ClientPage);