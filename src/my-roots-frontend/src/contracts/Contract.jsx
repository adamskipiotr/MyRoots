import React from 'react';
import {Button} from 'reactstrap';
import AdminAPI from '../admin/AdminAPI'


class Application extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            employeeID: this.props.employeeID,
        }
        this.deleteEmployeeAccount = this.deleteEmployeeAccount.bind(this);
        this.deactivateEmployeeAccount = this.deactivateEmployeeAccount.bind(this);

    }

    deleteEmployeeAccount = event => {
        event.preventDefault();
        AdminAPI.deleteEmployeeAccount(this.props.employeeID);
    }

    deactivateEmployeeAccount = event => {
        event.preventDefault();
        AdminAPI.deactivateEmployeeAccount(this.props.employeeID)
    }

    render() {
        return (
            <li className="item">
                <div className="content">
                    <div><b> {this.props.employeeName}</b> {this.props.activeContracts} {this.props.provided}
                        <div className="divider"/>
                        <Button color="secondary" size="lg" onClick={this.deactivateEmployeeAccount}>Deaktywuj</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.deleteEmployeeAccount}>Usuń</Button>
                    </div>
                </div>
            </li>
        )
    }
}

export default Application;