import React from 'react';
import Contract from './Contract';


class ContractsList extends React.Component {

    applicationToApplicationItem = contract => {
        const employeeID = contract.employeeID;
        const employeeName = contract.employeeName;
        const activeContracts = contract.activeContracts
        const providedInformations = contract.providedInformations;

        return <Contract key={employeeID} employeeID={employeeID} activeContracts={activeContracts}
                         employeeName={employeeName} provided={providedInformations}/>;
    }

    render() {
        return (
            <ul className="ui relaxed divided list selection">
                {this.props.contracts.map(this.applicationToApplicationItem)}
            </ul>
        )
    }
}

export default ContractsList;