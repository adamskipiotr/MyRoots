import React from 'react';
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import 'react-tree-graph/dist/style.css'
import * as d3 from 'd3'
import '../common/TreeGraph.css';
import "../common/Common.css"
import {Button} from "reactstrap";
import {saveSvgAsPng} from "save-svg-as-png"
import TreeService from '../Services/TreeService'


class ExampleFamilyTree extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.directionRef = React.createRef();
        this.valueRef = React.createRef();

        this.state = {
            isRendered: false,
            enableDownload: false,
            separation: 0,
            familyNames: [],
            familyMembers: [],
            family: "",
            buttonColor: "success",
            downloadButtonColor: "secondary",
            separationArray: [],
            enableModifyTreeStructure: true,
            data: [
                {
                    "child": "Samuel Nowak",
                    "parent": "",
                    "spouse": "Oktawia Szymańska",
                    "lifetime": "(1862-1920)",
                    "spouseLifetime": "(1862-1923)"
                },

                {
                    "child": "Maurycy Nowak",
                    "parent": "Samuel Nowak",
                    "spouse": "Nieznany",
                    "lifetime": "(1886-1950)",
                    "spouseLifetime": "(???-???)"
                },
                {
                    "child": "Jan Nowak",
                    "parent": "Samuel Nowak",
                    "spouse": "Klara Zielińska",
                    "lifetime": "(1889-1955)",
                    "spouseLifetime": "(1889-1950)"
                },

                {"child": "Stanisława Nowak", "parent": "Maurycy Nowak", "lifetime": "(1908-1979)"},

                {
                    "child": "Olgierd Nowak",
                    "parent": "Jan Nowak",
                    "spouse": "Anna Kot",
                    "lifetime": "(1912-1979)",
                    "spouseLifetime": "(1911-1986)"
                },
                {"child": "Róża Nowak", "parent": "Jan Nowak", "lifetime": "(1914-1987)"},
                {
                    "child": "Julita Nowak",
                    "parent": "Jan Nowak",
                    "spouse": "Igor Maj",
                    "lifetime": "(1910-1990)",
                    "spouseLifetime": "(1913-1988)"
                },
                {
                    "child": "Tobiasz Nowak",
                    "parent": "Jan Nowak",
                    "spouse": "Janina Mazur",
                    "lifetime": "(1918-1993)",
                    "spouseLifetime": "(1919-1996)"
                },

                {
                    "child": "Edward Nowak",
                    "parent": "Tobiasz Nowak",
                    "spouse": "Izabela Kowalska",
                    "lifetime": "(1963-2020)",
                    "spouseLifetime": "(1962-2020)"
                },
                {
                    "child": "Michał Nowak",
                    "parent": "Tobiasz Nowak",
                    "spouse": "Anna Wójcik",
                    "lifetime": "(1964-___)",
                    "spouseLifetime": "(1964-___)"
                },
                {
                    "child": "Katarzyna Nowak",
                    "parent": "Tobiasz Nowak",
                    "spouse": "Marcin Bąk",
                    "lifetime": "(1967-___)",
                    "spouseLifetime": "(1972-___)"
                },
                {
                    "child": "Kazimierz Nowak",
                    "parent": "Tobiasz Nowak",
                    "spouse": "Danuta Czech",
                    "lifetime": "(1967-2017)",
                    "spouseLifetime": "(1967-___)"
                },
                {
                    "child": "Janusz Nowak",
                    "parent": "Tobiasz Nowak",
                    "spouse": "Unknown",
                    "lifetime": "(1968-___)",
                    "spouseLifetime": "(1966-___)"
                },

                {"child": "Dagmara Nowak", "parent": "Edward Nowak", "lifetime": "(1986-___)"},
                {
                    "child": "Cezary Nowak",
                    "parent": "Edward Nowak",
                    "spouse": "Miriam Koziej",
                    "lifetime": "(1989-___)",
                    "spouseLifetime": "(1989-___)"
                },
                {"child": "Andrzej Nowak", "parent": "Edward Nowak", "lifetime": "(1992-___)"},
                {"child": "Magdal Nowak", "parent": "Michał Nowak", "lifetime": "(1989-___)"},
                {"child": "Tomasz Nowak", "parent": "Michał Nowak", "lifetime": "(1989-___)"},
                {"child": "Paweł Nowak", "parent": "Michał Nowak", "lifetime": "(1993-___)"},

                {"child": "Zofia Nowak", "parent": "Cezary Nowak", "lifetime": "(2018-___)"},
                {"child": "Helena Nowak", "parent": "Cezary Nowak", "lifetime": "(2020-___)"},
                {"child": "Antoni Nowak", "parent": "Cezary Nowak", "lifetime": "(2015-___)"},
            ],
            svg: ""
        }
        this.handleButtonTree = this.handleButtonTree.bind(this);
        this.handleDownloadButton = this.handleDownloadButton.bind(this);
    }

    componentWillUnmount = () => {
        d3.select("svg").remove();
    }

    changeHandler = event => {
        event.persist();
        this.setState(({
            [event.target.name]: event.target.value
        }))
    };

    handleDownloadButton = () => {
        saveSvgAsPng(document.getElementsByTagName("svg")[0], "Przykladowe_drzewo.png", {
            scale: 1,
            backgroundColor: "#FFFFFF"
        });
    }

    handleButtonTree = () => {
        this.setState(state => ({
            enableDownload: !state.enableDownload
        }));

        if (this.state.isRendered === false) {
            this.setState(({
                isRendered: true,
                buttonColor: "secondary",
                downloadButtonColor: "success"
            }));
            var dataStructure = d3.stratify()
                .id(function (d) {
                    return d.child;
                })
                .parentId(function (d) {
                    return d.parent;
                })(this.state.data);
            var treeStructure = d3.tree().size([2 * window.screen.width, 1]);
            var information = treeStructure(dataStructure);
            var height = dataStructure.height

            treeStructure = d3.tree().size([2 * window.screen.width, height * 100]);
            var informationTestLayout = treeStructure(dataStructure);

            var maxX = TreeService.findMaxXCoordinate(informationTestLayout, this.state.separationArray);
            this.svg = d3.select("p").attr("class", "graph-svg-component").append("svg")
                .attr("width", maxX + 300).attr("height", (height + 1) * 100)
                .append("g").attr("transform", "translate(90,50)");

            treeStructure = d3.tree().size([2 * window.screen.width, height * 100]);
            information = treeStructure(dataStructure);

            TreeService.correctPositions(information, this.state.separationArray)

            TreeService.addConnections1(this.svg, information)
            TreeService.addConnections2(this.svg, information)
            TreeService.addConnections3(this.svg, information)
            TreeService.addRectangles(this.svg, information)
            TreeService.addSpouseRectangles(this.svg, information)
            TreeService.addNames(this.svg, information)
            TreeService.addSpouseNames(this.svg, information)
        } else {
            d3.select("svg").remove();
            this.setState(({
                isRendered: false
            }));
        }
    }


    render() {
        return (
            <div className="info-background-nude">
                <div className="button-wrapper-emp">
                    <Button color="secondary" size="lg" onClick={() => this.props.history.push('/main')}>Powrót na
                        stronę główną</Button>
                    <div className="divider"/>
                    <Button color={this.state.buttonColor} size="lg" onClick={this.handleButtonTree}>Pokaż</Button>
                    <Button color={this.state.downloadButtonColor} size="lg"
                            disabled={this.state.enableDownload === false}
                            onClick={this.handleDownloadButton}>Pobierz</Button>
                </div>
                <p></p>
            </div>
        )
    }
}

export default withRouter(ExampleFamilyTree);
