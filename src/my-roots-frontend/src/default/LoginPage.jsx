import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css"
import AuthenticationService from '../Services/LoginService/AuthenticationService.js'
import {Button} from "reactstrap";
import {Col, Input} from 'reactstrap';


var redirect = "";

class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            login: "",
            password: "",
            hasLoginFailed: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleLogInClick = this.handleLogInClick.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleLogInClick(event) {
        event.preventDefault();

        AuthenticationService.executeBasicAuthentication(
            this.state.login,
            this.state.password
        )
            .then(function (response) {
                redirect = response.data;
            })
            .then(() => {
                if (redirect === "client") {
                    AuthenticationService.registerSuccessfulClientLogin(
                        this.state.login,
                        this.state.password
                    );
                } else if (redirect === "employee") {
                    AuthenticationService.registerSuccessfulEmployeeLogin(
                        this.state.login,
                        this.state.password
                    );
                } else if (redirect === "admin") {
                    AuthenticationService.registerSuccessfulAdminLogin(
                        this.state.login,
                        this.state.password
                    );
                }
                this.props.history.push("/" + redirect);
            })
            .catch(() => {
                this.setState({hasLoginFailed: true});
                OnLoginFailed(true);
            });
    }

    render() {
        return (
            <>
                <div className="large">MY ROOTS - LOGOWANIE DO SYSTEMU</div>
                <div className="info-background-nude">
                    <h3>Zaloguj się na swoje konto</h3>
                    <div className="button-wrapper-block">
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Login:
                                <Input name="login" id="loginInput"
                                       type="text" placeholder="Login"
                                       value={this.state.username}
                                       onChange={this.handleChange}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Hasło:
                                <Input name="password" id="passwordInput"
                                       type="password" placeholder="Hasło"
                                       value={this.state.password}
                                       onChange={this.handleChange}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </div>
                    <div className="button-wrapper-emp">
                        <Button color="secondary" size="lg" onClick={() => this.props.history.push('/main')}>Powrót na
                            stronę główną</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.handleLogInClick}>Zaloguj się</Button>
                    </div>
                </div>
            </>
        );
    }
}


function OnLoginFailed() {
    alert("BŁĄD LOGOWANIA")
}

export default withRouter(LoginPage);