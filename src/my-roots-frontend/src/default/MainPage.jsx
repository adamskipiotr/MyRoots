import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css"
import {Button} from "reactstrap";
import {RiTeamLine} from 'react-icons/ri';
import {GiFamilyTree} from 'react-icons/gi';


class MainPage extends React.Component {

    render() {
        return (
            <>
                <div className=" large">MY ROOTS - POZNAJ HISTORIĘ SWOJEJ RODZINY</div>
                <div className="info-background-nude">
                    <h3><GiFamilyTree/>Ocal od zapomnienia pamięć o Twoich przodkach</h3>
                    <div className="button-wrapper-main">
                        <Button color="secondary" size="lg" block onClick={() => this.props.history.push('/login')}>Zaloguj
                            się</Button>
                        <Button color="secondary" size="lg" block onClick={() => this.props.history.push('/example')}>Przykładowe
                            drzewo</Button>
                        <Button color="danger" size="lg" block onClick={() => this.props.history.push('/register')}>Zarejestruj
                            się</Button>
                    </div>
                    <p className="custom-p">Skorzystaj z usług oferowanych przez nasz zespół do skompletowania
                        informacji o swojej rodzinie - zgłoś się do nas z informacjami które posiadasz,
                        a my przeszukamy najróżniejsze archiwa by dostarczyć Ci Twoje drzewo genealogiczne sięgające
                        wiele pokoleń wstecz. Kto wie, może twój przodek
                        walczył pod Grunwaldem? A może w twoich żyłach płynie zagraniczna krew? Nasi eksperci włożą
                        pełnię swojego zaangażowania, by Twój rodowód nie miał przed Tobą żadnych tajmnic.</p>
                </div>
                <div className="info-background-green">
                    <h3><RiTeamLine/>Kim jesteśmy</h3>
                    <p className="custom-p">W nasz skład wchodzą miłośnicy historii oraz geneaologii.Na chwilę obecną
                        zatrudniamy 60 osób, porozdzielanych na 3-osobowe zespoły.Każdy z zespołów pobiera z listy
                        zgłoszeń prośbę o uzupełnienie
                        informacji o drzewie genealogicznym rodziny i jest odpowiedzialny za przebieg procesu od
                        początku aż do końca. Dzięki temu traktujemy każdego klienta indywidualnie, nawiązując z nim
                        prawdziwą relację współpracy opartej na
                        łączącej nas chęci do skompletowania informacji o jego rodzinie.Wszystkie osoby odpowiedzialne
                        za prowadzenie poszukiwań i weryfikację znalezionych materiałów posiadają wykształcenie
                        historyczne oraz umiejętność interpretacji
                        zarchiwizowanych dokumentów, dzięki czemu korzystając z naszych usługo możesz mieć pewność, że
                        ich efektem będzie wspaniałe i rzetelnie stworzone drzewo genealogiczne.</p>
                </div>
            </>
        );
    }
}

export default withRouter(MainPage);