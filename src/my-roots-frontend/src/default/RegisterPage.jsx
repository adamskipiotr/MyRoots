import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css"
import axios from "axios";
import {Button} from "reactstrap";
import {Col, Input} from 'reactstrap';


class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            client: {
                name: "",
                lastName: "",
                email: "",
                login: "",
                password: "",
                passwordRepeat: "",
            }
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleRegisterClick = this.handleRegisterClick.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    changeHandler = event => {
        event.persist();
        let value = event.target.value;
        this.setState(prevState => ({
            client: {...prevState.client, [event.target.name]: value}
        }))
    };


    handleRegisterClick(event) {
        event.preventDefault();
        return axios({

            method: "post",
            url: `http://localhost:8080/registerClient`,
            data: this.state.client,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function OnLoginFailed() {
            alert("Zostałeś zarejestrowany - wróć na stronę główną")
        })
    }


    render() {
        return (
            <>
                <div className=" large">MY ROOTS - REJESTRACJA DO SYSTEMU</div>
                <div className="info-background-nude">
                    <h3>Zarejestruj się by skorzystać z naszych usług</h3>
                    <div className="button-wrapper-block">
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Imię:
                                <Input name="name" id="nameInput"
                                       type="text" placeholder="Imię"
                                       value={this.state.client.name}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Nazwisko:
                                <Input name="lastName" id="lastnameInput"
                                       type="text" placeholder="Nazwisko"
                                       value={this.state.client.lastName}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Email:
                                <Input name="email" id="emailInput"
                                       type="email" placeholder="Email"
                                       value={this.state.client.email}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Login:
                                <Input name="login" id="loginInput"
                                       type="text" placeholder="Login"
                                       value={this.state.username}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Hasło:
                                <Input name="password" id="passwordInput"
                                       type="password" placeholder="Hasło"
                                       value={this.state.client.password}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col sm={{size: 6, order: 2, offset: 1}}>
                            <label>
                                Powtórz hasło:
                                <Input name="passwordRepeat" id="passwordRepeatInput"
                                       type="password" placeholder="Powtórz hasło"
                                       value={this.state.client.passwordRepeat}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </div>
                    <div className="button-wrapper-emp">
                        <Button color="secondary" size="lg" onClick={() => this.props.history.push('/main')}>Powrót na
                            stronę główną</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.handleRegisterClick}>Zarejestruj</Button>
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(RegisterPage);