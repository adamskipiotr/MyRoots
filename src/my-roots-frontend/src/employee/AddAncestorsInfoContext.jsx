import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css"
import {Button} from "reactstrap";
import {Input} from 'reactstrap';
import {Row, Col, Form} from 'react-bootstrap';
import './EmployeeAPI'
import EmployeeAPI from "./EmployeeAPI";


class AncestorsInfoContext extends React.Component {
    constructor(props) {
        super(props);
        this.selectedFamilyRef = React.createRef();
        this.state = {
            familyNames: [],
            familyMembers: [],
            familyMembersSingle: [],
            family: "",
            ancestor: {
                name: "",
                lastName: "",
                familyTreeName: "",
                birthDate: "",
                deathDate: "",
                isDescendant: "N",
                father: "",
                mother: "",
                spouse: "",
                applicationUUID: ""
            }
        };
        this.handleRegisterClick = this.handleRegisterClick.bind(this);
        this.handleFinishClick = this.handleFinishClick.bind(this);
        this.handleShowTreeClick = this.handleShowTreeClick.bind(this);
    }

    componentWillMount() {
        EmployeeAPI.getAllFamiliesClaimed()
            .then(response => {
                this.setState({familyNames: response.data})
            })
    }

    changeHandler = event => {
        event.persist();
        var value = event.target.value;
        if (event.target.name === "isDescendant") {
            if (event.target.checked) {
                value = "Y"
            } else {
                value = "N"
            }
        }
        this.setState(prevState => ({
            ancestor: {...prevState.ancestor, [event.target.name]: value}
        }))
    };

    handleFamilySelectorChange = event => {
        event.preventDefault();
        event.persist();
        let familyTreeName = event.target.value;
        this.setState({family: familyTreeName})
        this.setAncestorFamilyTreeName(familyTreeName);
        var uuidToSet = this.setAncestorFamilyTreeNameUUID(familyTreeName);
        EmployeeAPI.getAllFamilyMembers(familyTreeName, uuidToSet)
            .then(response => {
                this.setState({familyMembers: response.data.familyMemberList});
            })
        EmployeeAPI.getAllFamilyMembersSingle(familyTreeName, uuidToSet)
            .then(response => {
                this.setState({familyMembersSingle: response.data.familyMemberList});
            })
    };


    setAncestorFamilyTreeName(familyTreeName) {
        this.setState({familyTreeName: familyTreeName});
    }

    setAncestorFamilyTreeNameUUID(familyTreeName) {
        var applicationUUIDToSet = ""
        this.state.familyNames.forEach(function (entry) {
            if (familyTreeName === entry.familyTreeName) {
                applicationUUIDToSet = entry.applicationUUID
            }
        })
        this.state.ancestor.familyTreeName = familyTreeName;
        this.state.ancestor.applicationUUID = applicationUUIDToSet;
        return applicationUUIDToSet;
    }

    handleRegisterClick(event) {
        event.preventDefault();
        console.log(this.state.ancestor.spouse);
        EmployeeAPI.addNewPerson(this.state.ancestor)
            .then(response => {
                if (response.status === 200) {
                    alert("Dodano nowego członka rodziny")
                }
                var selectedFamilyName = document.getElementById("familySelect").value
                EmployeeAPI.getAllFamilyMembers(selectedFamilyName, this.state.ancestor.applicationUUID)
                    .then(response => {
                        this.setState({familyMembers: response.data.familyMemberList});
                    })
                EmployeeAPI.getAllFamilyMembersSingle(selectedFamilyName, this.state.ancestor.applicationUUID)
                    .then(response => {
                        this.setState({familyMembersSingle: response.data.familyMemberList});
                        this.state.ancestor.spouse = "";
                        this.state.ancestor.isDescendant = "N";
                    })
            })
    }

    handleShowTreeClick(event) {
        //TODO IMPLEMENT
    }

    handleFinishClick(event) {
        event.preventDefault();
        EmployeeAPI.finishCreatingTree(this.state.ancestor.familyTreeName)
            .then(response => {
                if (response.status === 200) {
                    alert("Drzewo zostało udostępnione klientowi")
                }
            });
    }


    render() {
        return (
            <div className="info-background-transparent">
                <h3>Wprowadź informacje o członku rodziny</h3>
                <div className="button-wrapper-center">
                    <Col xs={2} md={5}>
                        <label>
                            Rodzina:
                            <Form.Control as="select" style={{AlignSelf: 'center',}} name="family" id="familySelect"
                                          type="text" custom
                                          value={this.state.family}
                                          ref={this.selectedFamilyRef}
                                          onChange={this.handleFamilySelectorChange}>
                                <option value="" disabled defaultValue hidden>Wybierz rodzinę</option>
                                {this.state.familyNames.map(familyName =>
                                    <option key={familyName.applicationUUID}>{familyName.familyTreeName}</option>)}
                            </Form.Control>
                        </label>
                    </Col>
                </div>
                <div className="button-wrapper-block">
                    <Row>
                        <Col xs={1} md={3}>
                            <label>
                                Imię:
                                <Input name="name" id="nameInput"
                                       type="text" placeholder="Imię"
                                       value={this.state.ancestor.name}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col xs={1} md={3}>
                            <label>
                                Nazwisko:
                                <Input name="lastName" id="lastnameInput"
                                       type="text" placeholder="Nazwisko"
                                       value={this.state.ancestor.lastName}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={1} md={3}>
                            <label>
                                Data urodzenia:
                                <Input name="birthDate" id="birthDateInput"
                                       type="text" placeholder="Data urodzenia"
                                       value={this.state.ancestor.birthDate}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col xs={1} md={3}>
                            <label>
                                Data zgonu:
                                <Input name="deathDate" id="deathDateInput"
                                       type="text" placeholder="Data zgonu"
                                       value={this.state.ancestor.deathDate}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={1} md={3}>
                            <label>
                                Ojciec:
                                <Form.Control as="select" name="father" id="fatherSelect"
                                              type="text" custom
                                              value={this.state.ancestor.father}
                                              onChange={this.changeHandler}>
                                    <option value="" disabled defaultValue hidden>Wybierz ojca</option>
                                    {this.state.familyMembers.map(familyMember =>
                                        <option
                                            key={familyMember.name}>{familyMember.name} {familyMember.lastName}</option>)}
                                </Form.Control>
                            </label>
                        </Col>
                        <Col xs={1} md={3}>
                            <label>
                                Matka:
                                <Form.Control as="select" name="mother" id="motherSelect"
                                              type="text" custom
                                              value={this.state.ancestor.mother}
                                              onChange={this.changeHandler}>
                                    <option value="" disabled defaultValue hidden>Wybierz matkę</option>
                                    {this.state.familyMembers.map(familyMember =>
                                        <option
                                            key={familyMember.name}>{familyMember.name} {familyMember.lastName}</option>)}
                                </Form.Control>
                            </label>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={1} md={3}>
                            <label>
                                Małżonek:
                                <Form.Control as="select" name="spouse" id="spouseSelect"
                                              type="text" custom
                                              value={this.state.ancestor.spouse}
                                              onChange={this.changeHandler}>
                                    <option value="" disabled defaultValue hidden>Wybierz małżonka</option>
                                    {this.state.familyMembersSingle.map(familyMemberSingle =>
                                        <option
                                            key={familyMemberSingle.id}>{familyMemberSingle.name} {familyMemberSingle.lastName}</option>)}
                                </Form.Control>
                            </label>
                        </Col>
                    </Row>
                    <Col sm={{size: "auto"}}>
                        <Input name="isDescendant" id="isDescendantInput"
                               type="checkbox"
                               value={this.state.ancestor.isDescendant}
                               onChange={this.changeHandler}
                               style={{backgroundColor: '#f1f1f1'}}/>
                        <label htmlFor="isDescendantInput">Jest potomkiem</label>
                    </Col>
                </div>
                <div className="button-wrapper-inline">
                    <Button color="success" size="lg" onClick={this.handleRegisterClick}>Zarejestruj</Button>
                    <div className="divider"/>
                    <Button color="secondary" size="lg" onClick={() => this.props.history.push('/employeeTree')}>Wyświetl
                        drzewo</Button>
                    <div className="divider"/>
                    <Button color="danger" size="lg" onClick={this.handleFinishClick}>Zakończ tworzenie drzewa</Button>
                </div>
            </div>
        );
    }
}

export default withRouter(AncestorsInfoContext);