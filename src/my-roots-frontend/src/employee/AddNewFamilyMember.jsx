import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css";
import AncestorsInfoContext from './AddAncestorsInfoContext'


class NewFamilyMember extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addInfoPanel: true,
        };
        this.showAddInfoPanel = this.showAddInfoPanel.bind(this);
    }

    showAddInfoPanel() {
        this.setState(state => ({
            addInfoPanel: !state.addInfoPanel
        }));
    }

    render() {
        return (
            <>
                <AncestorsInfoContext/>
            </>
        );
    }
}

export default withRouter(NewFamilyMember);