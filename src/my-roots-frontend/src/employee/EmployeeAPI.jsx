import axios from "axios";


class EmployeeApi {

    getAllApplications() {
        return axios({
            method: "get",
            url: `http://localhost:8080/employee/checkRequests`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    claimApplication(applicationID) {
        return axios({
            method: "get",
            url: `http://localhost:8080/employee/claimRequest/` + applicationID,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (response.status === 200) {
                    alert("Przypisano do aplikacji")
                }
            });
    }

    getAllFamilyMembers(familyTreeName, applicationUUID) {
        return axios({
            method: "post",
            url: `http://localhost:8080/familyMember/getFamilyMembers/`,
            data: {
                "familyTreeName": familyTreeName,
                "applicationUUID": applicationUUID
            },
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    getAllFamilyMembersSingle(familyTreeName, applicationUUID) {
        return axios({
            method: "post",
            url: `http://localhost:8080/familyMember/getFamilyMembersSingle/`,
            data: {
                "familyTreeName": familyTreeName,
                "applicationUUID": applicationUUID
            },
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
    }

    getAllFamiliesClaimed() {
        return axios({
            method: "get",
            url: `http://localhost:8080/employee/getClaimedRequests`,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
    }

    getEmployeeFamilyTree(applicationUUID) {
        return axios({
            method: "get",
            url: `http://localhost:8080/employee/getFamilyTree/` + applicationUUID,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
    }

    addNewPerson(ancestor) {
        return axios({
            method: "post",
            url: `http://localhost:8080/employee/addFamilyMember`,
            data: ancestor,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
    }

    modifyPerson(ancestor) {
        return axios({
            method: "post",
            url: `http://localhost:8080/employee/modifyFamilyMember`,
            data: ancestor,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
    }

    correctTreeLayout(corrections) {
        return axios({
            method: "post",
            url: `http://localhost:8080/employee/correctTreeLayout`,
            data: corrections,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
    }

    finishCreatingTree(familyTreeName) {
        return axios({
            method: "get",
            url: `http://localhost:8080/employee/finishTree/` + familyTreeName,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
    }

    getTreeLayoutCorrections(familyTreeName) {
        return axios({
            method: "get",
            url: `http://localhost:8080/employee/getTreeLayoutCorrections/` + familyTreeName,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }
}

export default new EmployeeApi();