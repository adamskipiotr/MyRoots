import React from 'react';
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import 'react-tree-graph/dist/style.css'
import * as d3 from 'd3'
import '../common/TreeGraph.css'
import {Button, Input} from "reactstrap";
import {saveSvgAsPng} from "save-svg-as-png"
import {Row, Col, Form} from 'react-bootstrap';
import './EmployeeAPI'
import EmployeeAPI from "./EmployeeAPI";
import TreeService from '../Services/TreeService'


class EmployeeFamilyTree extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.directionRef = React.createRef();

        this.state = {
            isRendered: false,
            enableDownload: false,
            fetchedData: [],
            separation: 0,
            familyNames: [],
            familyMembers: [],
            data: [],
            family: "",
            buttonColor: "success",
            downloadButtonColor: "secondary",
            separationArray: [],
            enableModifyTreeStructure: true,
            svg: ""
        }
        this.handleButtonTree = this.handleButtonTree.bind(this);
        this.handleDownloadButton = this.handleDownloadButton.bind(this);
        this.toggleModifyTreeStructure = this.toggleModifyTreeStructure.bind(this);
        this.savePersonPositionModivication = this.savePersonPositionModivication.bind(this);
        this.handleSendChangesClick = this.handleSendChangesClick.bind(this);
        this.handleShowTreeClick = this.handleShowTreeClick.bind(this);
        this.getTreeLayoutCorrections = this.getTreeLayoutCorrections.bind(this);
    }


    componentWillUnmount = () => {
        d3.select("svg").remove();
    }


    componentWillMount() {
        EmployeeAPI.getAllFamiliesClaimed()
            .then(response => {
                this.setState({familyNames: response.data})
            })
    }

    changeHandler = event => {
        event.persist();
        var value = event.target.value;
        this.setState(prevState => ({
            ancestor: {...prevState.ancestor, [event.target.name]: value}
        }))
    };

    handleFamilySelectorChange = event => {
        event.preventDefault();
        event.persist();
        let familyTreeName = event.target.value;
        this.setState({family: familyTreeName})
        this.setAncestorFamilyTreeName(familyTreeName);
        this.setAncestorFamilyTreeNameUUID(familyTreeName);
    };

    setAncestorFamilyTreeName(familyTreeName) {
        this.setState({familyTreeName: familyTreeName});
    }

    setAncestorFamilyTreeNameUUID(familyTreeName) {
        var familyApplicationUUID = "";
        this.state.familyNames.forEach(function (entry) {
            if (familyTreeName === entry.familyTreeName) {
                familyApplicationUUID = entry.applicationUUID;
            }
        })
        this.setState({familyApplicationUUID: familyApplicationUUID});
    }

    handleShowTreeClick = event => {
        event.preventDefault();
        EmployeeAPI.getEmployeeFamilyTree(this.state.familyApplicationUUID)
            .then(response => {
                this.setState({data: response.data.familyMemberList}, () => {
                    this.getTreeLayoutCorrections(this.state.family)
                })
            })
    }

    getTreeLayoutCorrections(family) {
        EmployeeAPI.getTreeLayoutCorrections(family)
            .then(response => {
                this.setState({separationArray: response.data}, () => {
                    this.handleButtonTree()
                })
            })
    }

    changeHandler = event => {
        event.persist();
        this.setState(prevState => ({
            [event.target.name]: event.target.value
        }))
    };

    savePersonPositionModivication = () => {
        var personToMove = this.myRef.current.value;
        var direction = this.directionRef.current.value;
        var moveValue = document.getElementById("separationInput").value
        let correction = {
            "personToMove": personToMove,
            "direction": direction,
            "moveValue": moveValue
        }
        this.state.separationArray.push(correction)
        alert(personToMove + " zostanie przesunięty")
    }

    handleSendChangesClick(event) {
        event.preventDefault();
        EmployeeAPI.correctTreeLayout(this.state.separationArray)
    }

    toggleModifyTreeStructure = () => {
        this.setState(state => ({
            enableModifyTreeStructure: !state.enableModifyTreeStructure
        }));
    }

    handleDownloadButton = () => {
        saveSvgAsPng(document.getElementsByTagName("svg")[0], "Drzewo genealogiczne.png", {
            scale: 1,
            backgroundColor: "#FFFFFF"
        });
    }


    handleButtonTree = () => {
        this.setState(state => ({
            enableDownload: !state.enableDownload
        }));

        if (this.state.isRendered === false) {
            this.setState(({
                isRendered: true,
                buttonColor: "secondary",
                downloadButtonColor: "success"
            }));
            var dataStructure = d3.stratify()
                .id(function (d) {
                    return d.child;
                })
                .parentId(function (d) {
                    return d.parent;
                })(this.state.data);
            var treeStructure = d3.tree().size([2 * window.screen.width, 1]);
            var information = treeStructure(dataStructure);
            var height = dataStructure.height

            treeStructure = d3.tree().size([2 * window.screen.width, height * 100]);
            var informationTestLayout = treeStructure(dataStructure);

            var maxX = TreeService.findMaxXCoordinate(informationTestLayout, this.state.separationArray);
            this.svg = d3.select("p").attr("class", "graph-svg-component").append("svg")
                .attr("width", maxX + 600).attr("height", (height + 1) * 100)
                .append("g").attr("transform", "translate(90,50)");

            treeStructure = d3.tree().size([2 * window.screen.width, height * 100]);
            information = treeStructure(dataStructure);
            console.log(this.state.separationArray)
            TreeService.correctPositions(information, this.state.separationArray)

            TreeService.addConnections1(this.svg, information)
            TreeService.addConnections2(this.svg, information)
            TreeService.addConnections3(this.svg, information)
            TreeService.addRectangles(this.svg, information)
            TreeService.addSpouseRectangles(this.svg, information)
            TreeService.addNames(this.svg, information)
            TreeService.addSpouseNames(this.svg, information)
        } else {
            d3.select("svg").remove();
            this.setState(({
                isRendered: false
            }));
        }
    }

    render() {
        return (
            <div className="info-background-nude">
                <div className="button-wrapper-emp">
                    <Button color="secondary" size="lg" onClick={() => this.props.history.push('/employee')}>Powrót na
                        stronę pracownika</Button>
                    <div className="divider"/>
                    <div className="button-wrapper-center">
                        <Col xs={2} md={5}>
                            <label>
                                Rodzina:
                                <Form.Control as="select" style={{AlignSelf: 'center'}} name="family" id="familySelect"
                                              type="text" custom
                                              value={this.state.family}
                                              onChange={this.handleFamilySelectorChange}>
                                    <option value="" disabled defaultValue hidden>Wybierz rodzinę</option>
                                    {this.state.familyNames.map(familyName =>
                                        <option key={familyName.applicationUUID}>{familyName.familyTreeName}</option>)}
                                </Form.Control>
                            </label>
                        </Col>
                    </div>
                    <Button color={this.state.buttonColor} size="lg" onClick={this.handleShowTreeClick}>Pokaż</Button>
                    <Button color={this.state.downloadButtonColor} size="lg"
                            disabled={this.state.enableDownload === false}
                            onClick={this.handleDownloadButton}>Pobierz</Button>
                    <Button color="secondary" size="lg" onClick={this.toggleModifyTreeStructure}>Rozszesz</Button>
                </div>
                <div className="button-wrapper-center">
                    <Col xs={2} md={5} hidden={this.state.enableModifyTreeStructure}>
                        <label>
                            Edycja ustawienia osoby:
                            <Form.Control as="select" style={{AlignSelf: 'center',}} name="personToCorrect"
                                          id="personToCorrectSelect" type="text" custom ref={this.myRef}>
                                <option value="" disabled defaultValue hidden>Wybierz osobę</option>
                                {this.state.data.map(singlePerson =>
                                    <option key={singlePerson.child}>{singlePerson.child}</option>)}
                            </Form.Control>
                        </label>
                        <label>
                            Edycja ustawienia osoby:
                            <Form.Control as="select" style={{AlignSelf: 'center',}} name="direction"
                                          id="directionSelect" type="text" custom ref={this.directionRef}>
                                <option value="" disabled defaultValue hidden>Wybierz osobę</option>
                                <option value="lewo">Lewo</option>
                                <option value="prawo">Prawo</option>
                                <option value="gora">Góra</option>
                                <option value="dol">Dół</option>
                            </Form.Control>
                        </label>
                        <label>
                            Wartość przesunięcia:
                            <Input name="separation" id="separationInput"
                                   type="text" placeholder="0"
                                   ref={this.valueRef}
                                   value={this.state.separation}
                                   onChange={this.changeHandler}
                                   style={{backgroundColor: '#f1f1f1'}}/>
                        </label>
                        <br></br>
                        <Button color="secondary" size="lg" onClick={this.savePersonPositionModivication}
                                hidden={this.state.enableModifyTreeStructure}>Przesuń</Button>
                        <Button color="success" size="lg" onClick={this.handleSendChangesClick}
                                hidden={this.state.enableModifyTreeStructure}>Zapisz korekty</Button>
                    </Col>
                </div>
                <p></p>
            </div>
        )
    }
}

export default withRouter(EmployeeFamilyTree);
