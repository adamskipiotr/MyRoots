import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import '../common/Common.css';
import {Button} from "reactstrap";
import EmployeeApi from "./EmployeeAPI"
import NewApplications from "./NewApplications";
import NewFamilyMember from "./AddNewFamilyMember";
import ApplicationsList from '../applications/ApplicationsList';
import ModifyInfoContext from './ModifyInfoContext';
import AuthenticationService from '../Services/LoginService/AuthenticationService'


class EmployeePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            applications: [],
            status: "wrealizacji",
            addInfoPanel: false,
            modifyInfoPanel: false,
            newApplicationsButtonDescription: "Przeglądaj nowe prośby",
            newApplicationsButtonActive: false,
        };

        this.showAddInfoPanel = this.showAddInfoPanel.bind(this);
        this.showModifyInfoPanel = this.showModifyInfoPanel.bind(this);
        this.isnewApplicationsButtonActive = this.isnewApplicationsButtonActive.bind(this);
        this.toggleNewApplicationsButtonActive = this.toggleNewApplicationsButtonActive.bind(this);
        this.checkRequests = this.checkRequests.bind(this);
        this.logoutEmployee = this.logoutEmployee.bind(this);
    }

    showAddInfoPanel() {
        this.setState(state => ({
            addInfoPanel: !state.addInfoPanel
        }));
        this.setState(({
            modifyInfoPanel: false
        }));
    }

    clearUnhandledRequests = function () {

    }

    showModifyInfoPanel() {
        this.setState(state => ({
            modifyInfoPanel: !state.modifyInfoPanel
        }));
        this.setState(({
            addInfoPanel: false
        }));
    }

    toggleNewApplicationsButtonActive() {

        if (this.state.newApplicationsButtonDescription === "Przeglądaj nowe prośby") {
            this.setState(state => ({
                newApplicationsButtonDescription: "Ukryj nowe prośby"
            }));
        } else {
            this.setState(state => ({
                newApplicationsButtonDescription: "Przeglądaj nowe prośby"
            }));
        }

        this.setState(state => ({
            newApplicationsButtonActive: !state.newApplicationsButtonActive
        }));
        this.setState(({
            addInfoPanel: false
        }));
        this.setState(({
            modifyInfoPanel: false
        }));
    }

    isnewApplicationsButtonActive() {
        return this.state.newApplicationsButtonActive;
    }

    checkRequests(event) {
        event.preventDefault();
        this.toggleNewApplicationsButtonActive();
        if (true) {
            event.preventDefault();
            EmployeeApi.getAllApplications()
                .then(response => {
                    this.setState({applications: response.data})
                })
        }
    }

    logoutEmployee(event) {
        event.preventDefault();
        AuthenticationService.logoutEmployee();
        window.location.href = '/main'
    }

    render() {
        return (
            <>
                <div className=" large">MY ROOTS - STRONA PRACOWNIKA</div>
                <div className="info-background-nude">
                    <div className="button-wrapper-emp">
                        <Button color="secondary" size="lg"
                                onClick={this.checkRequests}>{this.state.newApplicationsButtonDescription}</Button>
                        <div className="divider"/>
                        <Button color="secondary" size="lg" onClick={this.showAddInfoPanel}>Rozbuduj drzewo
                            genealogiczne</Button>
                        <div className="divider"/>
                        <Button color="secondary" size="lg" onClick={this.showModifyInfoPanel}>Edytuj istniejącą
                            osobę</Button>
                        <div className="divider"/>
                        <Button color="danger" size="lg" onClick={this.logoutEmployee}>Wyloguj</Button>
                    </div>
                    <div className="info-background-transparent">
                        {this.state.newApplicationsButtonActive && <NewApplications/>}
                        {this.state.newApplicationsButtonActive &&
                        <ApplicationsList applications={this.state.applications}/>}
                    </div>
                    <div className="info-background-transparent">
                        {(this.state.addInfoPanel && <NewFamilyMember/>) || (this.state.modifyInfoPanel &&
                            <ModifyInfoContext/>)}
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(EmployeePage);