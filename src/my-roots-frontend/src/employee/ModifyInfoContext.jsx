import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import {Button} from "reactstrap";
import {Input} from 'reactstrap';
import {Row, Col, Form} from 'react-bootstrap';
import './EmployeeAPI'
import EmployeeAPI from "./EmployeeAPI";


class ModifyInfoContext extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            familyNames: [],
            familyMembers: [],
            family: "",
            ancestor: {
                name: "",
                lastName: "",
                familyTreeName: "",
                birthDate: "",
                deathDate: "",
                isDescendant: "N",
                person: "",
                applicationUUID: ""
            }
        };
        this.handleModifyPersonClick = this.handleModifyPersonClick.bind(this);
    }

    componentWillMount() {
        EmployeeAPI.getAllFamiliesClaimed()
            .then(response => {
                this.setState({familyNames: response.data})
            })
    }

    changeHandler = event => {
        event.persist();
        var value = event.target.value;
        this.setState(prevState => ({
            ancestor: {...prevState.ancestor, [event.target.name]: value}
        }))
    };

    handleFamilySelectorChange = event => {
        event.preventDefault();
        event.persist();
        let familyTreeName = event.target.value;
        this.setState({family: familyTreeName})
        this.setAncestorFamilyTreeName(familyTreeName);
        let applicationUUID = this.setAncestorFamilyTreeNameUUID(familyTreeName);

        EmployeeAPI.getAllFamilyMembers(familyTreeName, applicationUUID)
            .then(response => {
                this.setState({familyMembers: response.data.familyMemberList});
            })
    };

    setAncestorFamilyTreeName(familyTreeName) {
        this.setState({familyTreeName: familyTreeName});
    }

    setAncestorFamilyTreeNameUUID(familyTreeName) {
        var familyApplicationUUID = "";
        this.state.familyNames.forEach(function (entry) {
            if (familyTreeName === entry.familyTreeName) {
                familyApplicationUUID = entry.applicationUUID;
            }
        })
        this.state.ancestor.familyTreeName = familyTreeName;
        this.state.ancestor.applicationUUID = familyApplicationUUID;
        this.setState({familyApplicationUUID: familyApplicationUUID});
        return familyApplicationUUID;
    }

    handleModifyPersonClick(event) {
        event.preventDefault();
        EmployeeAPI.modifyPerson(this.state.ancestor)
            .then(response => {
                if (response.status === 200) {
                    alert("Dane osobowe zostały zmodyfikowane")
                }
                var selectedFamilyName = document.getElementById("familySelect").value
                EmployeeAPI.getAllFamilyMembers(selectedFamilyName)
                    .then(response => {
                        this.setState({familyMembers: response.data.familyMemberList});
                    })
            })
    }

    render() {
        return (
            <div className="info-background-transparent">
                <h3>Wprowadź informacje o członku rodziny</h3>
                <div className="button-wrapper-center">
                    <Col xs={2} md={5}>
                        <label>
                            Rodzina:
                            <Form.Control as="select" style={{AlignSelf: 'center'}} name="family" id="familySelect"
                                          type="text" custom
                                          value={this.state.family}
                                          onChange={this.handleFamilySelectorChange}>
                                <option value="" disabled defaultValue hidden>Wybierz rodzinę</option>
                                {this.state.familyNames.map(familyName =>
                                    <option key={familyName.applicationUUID}>{familyName.familyTreeName}</option>)}
                            </Form.Control>
                        </label>
                    </Col>
                    <Col xs={2} md={5}>
                        <label>
                            Osoba:
                            <Form.Control as="select" style={{AlignSelf: 'center'}} name="person" id="personSelect"
                                          type="text" custom
                                          value={this.state.ancestor.person}
                                          onChange={this.changeHandler}>
                                <option value="" disabled defaultValue hidden>Wybierz osobę</option>
                                {this.state.familyMembers.map(familyMember =>
                                    <option
                                        key={familyMember.name}>{familyMember.name} {familyMember.lastName}</option>)}
                            </Form.Control>
                        </label>
                    </Col>
                </div>
                <div className="button-wrapper-block">
                    <Row>
                        <Col xs={1} md={3}>
                            <label>
                                Imię:
                                <Input name="name" id="nameInput"
                                       type="text" placeholder="Imię"
                                       value={this.state.ancestor.name}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col xs={1} md={3}>
                            <label>
                                Nazwisko:
                                <Input name="lastName" id="lastnameInput"
                                       type="text" placeholder="Nazwisko"
                                       value={this.state.ancestor.lastName}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={1} md={3}>
                            <label>
                                Data urodzenia:
                                <Input name="birthDate" id="birthDateInput"
                                       type="text" placeholder="Data urodzenia"
                                       value={this.state.ancestor.birthDate}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                        <Col xs={1} md={3}>
                            <label>
                                Data zgonu:
                                <Input name="deathDate" id="deathDateInput"
                                       type="text" placeholder="Data zgonu"
                                       value={this.state.ancestor.deathDate}
                                       onChange={this.changeHandler}
                                       style={{backgroundColor: '#f1f1f1'}}/>
                            </label>
                        </Col>
                    </Row>
                </div>
                <div className="button-wrapper-modify">
                    <Button color="success" size="lg" onClick={this.handleModifyPersonClick}>Zapisz zmianę</Button>
                </div>
            </div>
        );
    }
}

export default withRouter(ModifyInfoContext);