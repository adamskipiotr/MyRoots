import React from "react";
import {BrowserRouter as Router, withRouter} from "react-router-dom";
import "../common/Common.css";
import EmployeeApi from "./EmployeeAPI"


class NewApplications extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            applications: [],
            status: "wrealizacji",
            addInfoPanel: true,
            newApplicationsButtonDescription: "Przeglądaj nowe prośby",
            newApplicationsButtonActive: true
        };

        this.showAddInfoPanel = this.showAddInfoPanel.bind(this);
        this.isnewApplicationsButtonActive = this.isnewApplicationsButtonActive.bind(this);
        this.toggleNewApplicationsButtonActive = this.toggleNewApplicationsButtonActive.bind(this);
        this.checkRequests = this.checkRequests.bind(this);
    }

    showAddInfoPanel() {
        this.setState(state => ({
            addInfoPanel: !state.addInfoPanel
        }));
    }

    toggleNewApplicationsButtonActive() {

        if (this.state.newApplicationsButtonDescription === "Przeglądaj nowe prośby") {
            this.setState(state => ({
                newApplicationsButtonDescription: "Ukryj nowe prośby"
            }));
        } else {
            this.setState(state => ({
                newApplicationsButtonDescription: "Przeglądaj nowe prośby"
            }));
        }

        this.setState(state => ({
            newApplicationsButtonActive: !state.newApplicationsButtonActive
        }));
    }

    isnewApplicationsButtonActive() {
        return this.state.newApplicationsButtonActive;
    }

    checkRequests(event) {
        event.preventDefault();
        this.toggleNewApplicationsButtonActive();
        if (this.isnewApplicationsButtonActive()) {
            event.preventDefault();
            EmployeeApi.getAllApplications()
        }
    }

    render() {
        return (
            <>
                Nierozpoczęte wnioski
            </>
        );
    }
}

export default withRouter(NewApplications);