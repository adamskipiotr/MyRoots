package com.pada.myroots.admin;

import com.pada.myroots.employee.Employee;
import com.pada.myroots.employee.dto.EmployeeDTO;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

@AutoConfigureMockMvc
//need this in Spring Boot test
class AdminControllerTest {

    @MockBean
    AdminService adminService;

    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;


    @WithMockUser(username = "ADMIN", roles = {"ADMIN"})
    @Test
    void shouldPostNewEmployeeDataWhenRequestWithBodySent() throws Exception {
        // given
        EmployeeDTO employeeDTO = new EmployeeDTO("testName", "testLastName", "testLogin", "testPassword", "testEmail");
        Employee employee = new Employee();
        employee.setLogin(employeeDTO.getLogin());

        // when
        // then
        mockMvc.perform(post("/admin/addEmployee")
                .content(mapper.writeValueAsString(employeeDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}