package com.pada.myroots.admin;

import com.pada.myroots.employee.Employee;
import com.pada.myroots.employee.EmployeeRepository;
import com.pada.myroots.employee.dto.EmployeeDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;


@SpringBootTest
class AdminServiceTest {

    @Autowired
    AdminService adminService;
    @MockBean
    private EmployeeRepository employeeRepository;

    @MockBean
    private AdminRepository adminRepository;


    @SuppressWarnings("deprecation")
    @Test
    void shouldPerformSingleSaveQueryWhenOneEmployeeCreatedByAdmin() throws NoSuchFieldException {
        // given
        FieldSetter.setField(adminService, adminService.getClass().getDeclaredField("passwordEncoder"), NoOpPasswordEncoder.getInstance());
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setPassword("password");
        employeeDTO.setName("name");
        employeeDTO.setEmail("email");
        employeeDTO.setLastName("lastname");
        employeeDTO.setLogin("login");
        Employee employee = new Employee(null, "name", "lastname", "login", NoOpPasswordEncoder.getInstance().encode("password"), "email", true, "ROLE_EMPLOYEE", null);
        //when
        adminService.addNewEmployee(employeeDTO);
        // then
        Mockito.verify(employeeRepository, times(1)).save(employee);
    }

    @Test
    void shouldReturnValidInstanceOfAdminClassWhenSearchForAccountSavedInDatabase() {
        // given
        Long expectedID = 1L;
        Admin mockAdmin = new Admin();
        mockAdmin.setId(1L);
        mockAdmin.setLogin("Admin");
        // when
        when(adminRepository.findByLogin("Admin")).thenReturn(java.util.Optional.of(mockAdmin));
        Admin adminFound = adminService.findByLogin("Admin");
        Long actualID = adminFound.getId();
        // then
        Assert.assertEquals(expectedID, actualID);
    }

    @Test
    void shouldReturnNullWhenSearchForNotExistingAccount() {
        // given
        Admin mockAdmin = new Admin();
        mockAdmin.setId(1L);
        mockAdmin.setLogin("Admin");
        // when
        when(adminRepository.findByLogin("Admin")).thenReturn(java.util.Optional.of(mockAdmin));
        Admin adminFound = adminService.findByLogin("NonExisting");
        // then
        Assert.assertNull(adminFound);
    }
}