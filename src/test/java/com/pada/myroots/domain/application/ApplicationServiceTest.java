package com.pada.myroots.domain.application;

import com.pada.myroots.client.Client;
import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.employee.Employee;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

@SpringBootTest
class ApplicationServiceTest {

    @Autowired
    ApplicationService applicationService;
    @MockBean
    ApplicationRepository applicationRepository;

    private final List<Application> applicationList = new ArrayList<>();
    private List<ClaimedApplicactionDto> claimedApplicactionDtos = new ArrayList<>();

    @BeforeEach
    public void createApplicationList() {
        applicationList.clear();
        Application firstApplication = new Application();
        UUID uuid = UUID.randomUUID();
        firstApplication.setApplicationUUID(uuid);
        firstApplication.setId(1L);
        firstApplication.setFamilyTreeName("firstFamily");
        firstApplication.setStatus("wrealizacji");
        Application secondApplication = new Application();
        uuid = UUID.randomUUID();
        secondApplication.setId(2L);
        secondApplication.setApplicationUUID(uuid);
        secondApplication.setFamilyTreeName("secondFamily");
        secondApplication.setStatus("zakonczony");
        Application thirdApplication = new Application();
        uuid = UUID.randomUUID();
        thirdApplication.setId(3L);
        thirdApplication.setApplicationUUID(uuid);
        thirdApplication.setFamilyTreeName("thirdFamily");
        thirdApplication.setStatus("nierozpoczety");
        applicationList.add(firstApplication);
        applicationList.add(secondApplication);
        applicationList.add(thirdApplication);
    }

    @BeforeEach
    public void createExpectedClaimedApplicationsList() {
        claimedApplicactionDtos = applicationList
                .stream()
                .map(ClaimedApplicactionDto::new)
                .collect(Collectors.toList());
    }

    @Test
    void getAllUnhandledApplications() {
    }

    @Test
    void createApplicationRequest() {
    }

    @Test
    void shouldPerformOneAssignToApplicationQueryWhenAnEmployeeAddedToApplication() {
        // given
        UUID uuid = UUID.randomUUID();
        Application applicationToUpdate = new Application();
        applicationToUpdate.setApplicationUUID(uuid);
        applicationRepository.save(applicationToUpdate);
        Employee employee = new Employee();
        // when
        applicationService.assignToApplication(uuid.toString(), employee);
        // then
        Mockito.verify(applicationRepository, times(1)).assignToApplication(uuid, employee);
    }

    @Test
    void shouldReturnListOfThreeClaimedRequestsForEmployeeWorkingOnThreeApplications() {
        // given
        Employee activeEmployee = new Employee();

        // when
        when(applicationRepository.findAllClaimedByEmployee(activeEmployee)).thenReturn(this.applicationList);
        List<ClaimedApplicactionDto> result = applicationService.getClaimedRequests(activeEmployee);

        // then
        Assert.assertEquals(this.claimedApplicactionDtos, result);
    }

    @Test
    void shouldReturnNiewyslanyValueForNewlyCreatedClient() {
        // given
        Client emptyClient = new Client();
        // when
        String result = applicationService.getClientRequestStatus(emptyClient);
        // then
        Assert.assertEquals("niewyslany", result);
    }

    @Test
    void shouldReturnWrealizacjiForClientWithApplicationBeingHandled() {
        // given
        Client awaitingClient = new Client();
        Application application = new Application();
        application.setStatus("wrealizacji");
        awaitingClient.setApplication(application);
        // when
        String result = applicationService.getClientRequestStatus(awaitingClient);
        // then
        Assert.assertEquals("wrealizacji", result);
    }

    @Test
    void shouldPerformSingleFinishTreeQueryWhenThreeMarkedAsFinished() {
        // given
        UUID uuid = UUID.randomUUID();
        Application applicationToUpdate = new Application();
        applicationToUpdate.setFamilyTreeName("TEST_FAMILY");
        applicationToUpdate.setApplicationUUID(uuid);
        applicationRepository.save(applicationToUpdate);
        // when
        applicationService.finishCreatingTree("TEST_FAMILY");
        // then
        Mockito.verify(applicationRepository, times(1)).finishCreatingTree("TEST_FAMILY");
    }
}