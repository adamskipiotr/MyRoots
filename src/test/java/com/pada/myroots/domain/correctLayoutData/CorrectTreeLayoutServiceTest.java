package com.pada.myroots.domain.correctLayoutData;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

@SpringBootTest
class CorrectTreeLayoutServiceTest {

    @Autowired
    CorrectTreeLayoutService correctTreeLayoutService;

    @MockBean
    CorrectTreeLayoutRepository correctTreeLayoutRepository;

    private final List<CorrectTreeLayoutDTO> correctTreeLayoutDTOS = new ArrayList<>();

    @BeforeEach
    public void createCorrectionList() {
        CorrectTreeLayoutDTO firstCorrection = new CorrectTreeLayoutDTO();
        firstCorrection.setDirection("lewo");
        firstCorrection.setMoveValue("5");
        firstCorrection.setFamilyTreeName("familyTest");
        firstCorrection.setPersonToMove("personTest");
        correctTreeLayoutDTOS.add(firstCorrection);
        CorrectTreeLayoutDTO secondCorrection = new CorrectTreeLayoutDTO();
        secondCorrection.setDirection("prawo");
        secondCorrection.setMoveValue("10");
        secondCorrection.setFamilyTreeName("familyTest");
        secondCorrection.setPersonToMove("personTest");
        correctTreeLayoutDTOS.add(secondCorrection);
        CorrectTreeLayoutDTO thirdCorrection = new CorrectTreeLayoutDTO();
        thirdCorrection.setDirection("gora");
        thirdCorrection.setMoveValue("15");
        thirdCorrection.setFamilyTreeName("familyTest");
        thirdCorrection.setPersonToMove("personTest");
        correctTreeLayoutDTOS.add(thirdCorrection);
    }

    @Test
    void shouldPerformSaveQueryThreeTimesWhenListOfThreeCorrectionsGivenToSave() {
        // given
        // when
        correctTreeLayoutService.addCorrections(correctTreeLayoutDTOS);
        // then
        Mockito.verify(correctTreeLayoutRepository, times(3)).save(any(CorrectTreeLayoutData.class));
    }

    @Test
    void should() {
        // given
        String familyName = "familyTest";
        List<CorrectTreeLayoutData> resultListDataExpected = new LinkedList<>();
        for (CorrectTreeLayoutDTO dto : correctTreeLayoutDTOS) {
            CorrectTreeLayoutData data = new CorrectTreeLayoutData();
            data.setMoveValue(dto.getMoveValue());
            data.setDirection(dto.getDirection());
            data.setPersonToMove(dto.getPersonToMove());
            data.setFamilyTreeName(dto.getFamilyTreeName());
            resultListDataExpected.add(new CorrectTreeLayoutData());
        }
        // when
        when(correctTreeLayoutRepository.findAllCorrectionForFamilyTree(familyName)).thenReturn(resultListDataExpected);
        List<CorrectTreeLayoutData> resultList = correctTreeLayoutService.getAllCorrectionsForFamilyTree(familyName);
        // then
        Assert.assertEquals(resultListDataExpected, resultList);
    }

    @Test
    void getAllCorrectionsForFamilyTreeClient() {
    }
}