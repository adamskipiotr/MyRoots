package com.pada.myroots.domain.familyMember;

import com.pada.myroots.domain.application.DTO.ClaimedApplicactionDto;
import com.pada.myroots.domain.familyMember.DTO.FamilyMemberDTO;
import com.pada.myroots.domain.familyMember.wrapper.FamilyMemberWrapper;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@SpringBootTest
class FamilyMemberServiceTest {

    @Autowired
    FamilyMemberService familyMemberService;

    @MockBean
    FamilyMemberRepository familyMemberRepository;

    private final List<FamilyMember> familyMembers = new ArrayList<>();

    @BeforeEach
    public void createFamilyMemberList() {
        FamilyMember firstFamilyMember = new FamilyMember();
        firstFamilyMember.setName("firstName");
        firstFamilyMember.setLastName("firstLastName");
        firstFamilyMember.setBirthDate("1900");
        firstFamilyMember.setDeathDate("1990");
        FamilyMember secondFamilyMember = new FamilyMember();
        secondFamilyMember.setName("secondName");
        secondFamilyMember.setLastName("secondLastName");
        secondFamilyMember.setBirthDate("1900");
        secondFamilyMember.setDeathDate("1990");
        FamilyMember thirdFamilyMember = new FamilyMember();
        thirdFamilyMember.setName("thirdName");
        thirdFamilyMember.setLastName("thirdLastName");
        thirdFamilyMember.setBirthDate("1900");
        thirdFamilyMember.setDeathDate("1990");
        familyMembers.add(firstFamilyMember);
        familyMembers.add(secondFamilyMember);
        familyMembers.add(thirdFamilyMember);
    }

    @Test
    void shouldPerformSingleSaveQueryWhenSingleFamilyMemberAdded() {
        FamilyMemberDTO familyMemberDTO = new FamilyMemberDTO("testName", "testLastName", "1900", "1990");
        familyMemberDTO.setApplicationUUID("01bfb767-be4b-42e3-b256-e73dc408448a");
        familyMemberDTO.setFather("");
        familyMemberDTO.setMother("");
        familyMemberDTO.setSpouse("");
        familyMemberService.addNewFamilyMember(familyMemberDTO);
        Mockito.verify(familyMemberRepository, times(1)).save(any(FamilyMember.class));
    }

    @Test
    void shouldReturnListOfTestFamilyMembersWhenSearchedWithFamilyTreeNameTestFamily() {
        // given
        ClaimedApplicactionDto dto = new ClaimedApplicactionDto("testFamily", "e023ec73-8e71-4217-9040-0c5be5b7ab7a");

        LinkedList<FamilyMemberDTO> expectedFamilyMembersTestList = new LinkedList<>();
        expectedFamilyMembersTestList.add(new FamilyMemberDTO("firstName", "firstLastName", "1900", "1990"));
        expectedFamilyMembersTestList.add(new FamilyMemberDTO("secondName", "secondLastName", "1900", "1990"));
        expectedFamilyMembersTestList.add(new FamilyMemberDTO("thirdName", "thirdLastName", "1900", "1990"));

        FamilyMemberWrapper expectedWrapper = new FamilyMemberWrapper();
        expectedWrapper.setFamilyMemberList(expectedFamilyMembersTestList);

        //when
        when(familyMemberRepository.findAllByFamilyTreeName("testFamily", UUID.fromString("e023ec73-8e71-4217-9040-0c5be5b7ab7a"))).thenReturn(familyMembers);
        FamilyMemberWrapper resultWrapper = familyMemberService.getAllFamilyMembers(dto);
        // then
        Assert.assertEquals(expectedWrapper.getFamilyMemberList(), resultWrapper.getFamilyMemberList());
    }
}