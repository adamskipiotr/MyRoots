package com.pada.myroots.employee;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
class EmployeeServiceTest {

    @Autowired
    EmployeeService employeeService;
    @MockBean
    private EmployeeRepository employeeRepository;


    @Test
    void getAllEmployeesStats() {
    }


    @Test
    void deleteEmployeeAccount() {
        //given
        final Employee entity = new Employee();
        entity.setId(1L);
        Optional<Employee> optionalEntityType = Optional.of(entity);
        Mockito.when(employeeRepository.findById(1L)).thenReturn(optionalEntityType);
        // when
        employeeService.deleteEmployeeAccount(2L);

        // then
        Mockito.verify(employeeRepository, times(1)).delete(entity);
    }
}